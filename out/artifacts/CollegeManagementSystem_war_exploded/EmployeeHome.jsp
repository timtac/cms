<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/29/2015
  Time: 8:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null){
        String status = (String) session.getAttribute("employeelogin");
        if(status == null){
            response.sendRedirect("/Login.jsp");
        }
    }
    else{
        response.sendRedirect("Login.jsp");
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>
    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <%
            String firstname = (String) session.getAttribute("first-name");
            String lastname = (String) session.getAttribute("last-name");
            String post = (String) session.getAttribute("post");
            String hire = (String) session.getAttribute("hire");
            String sex = (String) session.getAttribute("sex");
            String age = (String) session.getAttribute("age");
            String address = (String) session.getAttribute("address");
            String photo = (String) session.getAttribute("photo");
        %>
        <div id="register-content">
                <img src="" alt="" width="250" height="250"><center><h3>EMPLOYEE HOME</h3></center>
            <table>

                <tr>
                    <th>NAME:</th>
                    <td><b><%=firstname%></b> , <%=lastname%>.</td>
                </tr>
                <tr>
                    <th>POST:</th>
                    <td><b><%=post%></b></td>
                </tr>
                <tr>
                    <th>HIRE DATE:</th>
                    <td><%=hire%></td>
                </tr>
                <tr>
                    <th>USERNAME:</th>
                    <td><b><%=username%></b></td>
                </tr>

                <tr>
                    <th>HOME ADDRESS:</th>
                    <td><b><%=address%></b></td>
                </tr>
                <tr>
                    <th>DATE OF BIRTH:</th>
                    <td><b><%=age%></b></td>
                </tr>
                <tr>
                    <th>SEX:</th>
                    <td><b><%=sex%></b></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script >
    $(document).ready(function(){
        $('#firstname').focus()
    })
</script>
</body>
</html>
