<%@ page import="Controllers.ImageUpload" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/29/2015
  Time: 8:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null){
        String status = (String) session.getAttribute("adminlogin");
        if(status == null){
            response.sendRedirect("Login.jsp");
        }
    }
    else{
        response.sendRedirect("Login.jsp");
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>
    <%
       String firstName = (String) session.getAttribute("firstname");
        String lastName = (String) session.getAttribute("lastname");
        String post = (String) session.getAttribute("post");
        String age = (String) session.getAttribute("age");
        String sex = (String) session.getAttribute("sex");
        String address = (String) session.getAttribute("address");
        String username = (String) session.getAttribute("username");
        String photo = (String) session.getAttribute("photo");
        String imageUrl = "/UploadDownloadImage?fileName="+photo;
    %>
    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="registerEmployees.jsp">Register Employee</a></li>
                            <li><a href="EmployeeIDForm.jsp">Employee ID</a></li>

                        </ul>
                    </li>
                    <li><a href="EmployeeSalaryForm.jsp">Salary</a></li>
                    <li><a href="AllEmployee.jsp">All Employee(s)</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchEmployee.jsp"> Employee(s)</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=firstName%>,<%=lastName%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">

        <div id="register-content">

           <img src="<%=imageUrl%>" width="29px" height="29px"> <h3>EMPLOYEE DETAILS</h3>
            <table>

                <tr>
                    <th>First Name: </th>
                    <td><%=firstName%></td>
                </tr>
                <tr>
                    <th>Last Name: </th>
                    <td><%=lastName%></td>
                </tr>
                <tr>
                    <th>POST HELD:</th>
                    <td><%=post%></td>
                </tr>

                <tr>
                    <th>USER ID:</th>
                    <td><%=username%></td>
                </tr>
                <tr>
                    <th>SEX: </th>
                    <td><%=sex%></td>
                </tr>
                <tr>
                    <th>D.O.B: </th>
                    <td><%=age%></td>
                </tr>
                <tr>
                    <th>ADDRESS:</th>
                    <td><address><%=address%></address></td>
                </tr>
            </table>

        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<style>
    table{
        border: 0px solid #000000;
        width: 100%;
        margin: 10px;
    }
    th {
        border: 0px;
        width: 50%;
        height: 50px;
    }
    td {
        border: 0px;
        width: 50%;
        height: 50px;
    }
</style>
<script>
    $(document).ready(function(){

    })
</script>
</body>
</html>

