<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/15/2015
  Time: 11:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Models.EmployeeId" %>

<jsp:useBean id="ID" scope="session" class="Models.EmployeeId">
    <jsp:setProperty name="ID" property="*"></jsp:setProperty>
</jsp:useBean>

<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>
    <%
        String username = (String) session.getAttribute("username");
    %>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="AdminHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="registerEmployees.jsp">Register Employee</a></li>
                            <li><a href="EmployeeIDForm.jsp">Employee ID</a></li>

                        </ul>
                    </li>
                    <li><a href="EmployeeSalaryForm.jsp">Salary</a></li>
                    <li><a href="AllEmployee.jsp">All Employee(s)</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchEmployee.jsp"> Employee(s)</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div id="main-content">
        <div id="register-content">
               <img src="C:/Users/user/IdeaProjects/CollegeManagementSystem/out/artifacts/CollegeManagementSystem_war_exploded/images/IMG_0014.JPG" /> EMPLOYEE IDENTITY CARD
            <table>
                <tr>
                    <th>FULL NAME:</th>
                    <td><jsp:getProperty name="ID" property="fullname"></jsp:getProperty></td>
                </tr>
                <tr>
                    <th>POST HELD:</th>
                    <td><jsp:getProperty name="ID" property="postHeld"></jsp:getProperty></td>
                </tr>
                <tr>
                    <th>EMPLOYEE ID:</th>
                    <td><jsp:getProperty name="ID" property="empIdNo"></jsp:getProperty></td>
                </tr>
                <tr>
                    <th>NEXT OF KIN:</th>
                    <td><jsp:getProperty name="ID" property="nextOfKin"></jsp:getProperty></td>
                </tr>

            </table>

            <div class="col-sm-3 col-md-3 col-xs-12 pull-right">
                <button type="submit" onclick="printDiv('register-content')" id="prtbtn" class="btn btn-success"><i class="glyphicon glyphicon-print"></i>Print Report</button>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<style>
    table{
        border: 0px solid #000000;
        width: 100%;
        margin: 10px;
    }
    th {
        border: 0px;
        width: 50%;
        height: 50px;
    }
    td {
        border: 0px;
        width: 50%;
        height: 50px;
    }
</style>
</body>
</html>


