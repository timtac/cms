<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11/26/2015
  Time: 11:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="custom/custom.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>

        <link rel="stylesheet" type="text/css" href="custom/custom.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>
    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div class="pull-right ">
            <select class="col-lg-12">
                <option value="">Search</option>
                <option value="">Student Details</option>
                <option value="">Student Report</option>
            </select>

        </div>
        <div id="register-content">

            <form  name="student" action="/student" onsubmit="return submitStudent()" method="POST" enctype="multipart/form-data" class="form-horizontal" >

                <h3>ENROL STUDENT</h3>

                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12">Firstname:</label>
                        <div class="col-sm-6 col-md-6 col-xs-12">
                            <input type="text" name="firstname" id="firstname" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12">Lastname:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="lastname" id="lastname" />
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Faculty:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="course" id="course" placeholder="" />
                        </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Department:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="department" id="department"  placeholder="" />
                        </div>
                    </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12" >Matric. No.:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="matricNo" id="matricNo"  placeholder="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12" > Age:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="date" name="age" id="age" max="2012-12-31" />
                    </div>
                </div>
                <div class="form-group">
                <label class="control-label col-sm-3 col-md-3 col-xs-12"> Sex:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="radio" name="sex" id="sex" value="M" checked />Male
                        <input type="radio" name="sex"  value="F" />Female
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">
                            Address:
                    </label>
                    <textarea size="30" placeholder="Home Address" name="address" id="address" style="width: 277px; height: 88px" ></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Choose Image:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="file" id="file" />
                    </div>
                </div>
                <div class="form-group pull-right">
                    <input type="submit" value="Submit" class="btn btn-success btn-block">
                    <input type="reset" value="Reset" class="btn btn-block btn-default" >
                </div>
            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script >
    $(document).ready(function(){
        $('#firstname').focus()
    })
</script>
</body>
</html>
