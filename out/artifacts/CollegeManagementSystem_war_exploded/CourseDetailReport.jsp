<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(session != null){
        String state = (String) session.getAttribute("course");
        if(state == null){
            response.sendRedirect("/CourseRegistration.jsp");
        }
    }
    else{
        response.sendRedirect("/CourseRegistration.jsp");
    }
%>

<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <%
        String matricNo = (String) session.getAttribute("matricNo");
        String level = (String) session.getAttribute("level");
        String semester = (String) session.getAttribute("semester");
        String contactSession = (String) session.getAttribute("session");
        String department = (String) session.getAttribute("department");
        String course1 = (String) session.getAttribute("course1");
        String unit1 = (String) session.getAttribute("unit1");
        String course2 = (String) session.getAttribute("course2");
        String unit2 = (String) session.getAttribute("unit2");
        String course3 = (String) session.getAttribute("course3");
        String unit3 = (String) session.getAttribute("unit3");
        String course4 = (String) session.getAttribute("course4");
        String unit4 = (String) session.getAttribute("unit4");
        String course5 = (String) session.getAttribute("course5");
        String unit5 = (String) session.getAttribute("unit5");
        String course6 = (String) session.getAttribute("course6");
        String unit6 = (String) session.getAttribute("unit6");
        String course7 = (String) session.getAttribute("course7");
        String unit7 = (String) session.getAttribute("unit7");
        String course8 = (String) session.getAttribute("course8");
        String unit8 = (String) session.getAttribute("unit8");
    %>

    <div id="main-content">
        <div id="register-content">

            <table>
                <thead>
                <tr>
                    <th>Matric. No.: <%=matricNo%></th>
                    <th>Semester: <%=semester%></th>
                </tr>
                <tr>
                    <th>Level: <%=level%></th>
                    <th>Session: <%=contactSession%></th>
                </tr>
                <tr>
                    <th>Department: <%=department%></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

                    <br/>
            <br />
<br />
            <table>
                <tr>
                    <th>Course</th>
                    <th>Grade</th>
                </tr>

                <tbody>

                    <tr>
                        <td><%=course1%></td>
                        <td><%=unit1%></td>
                    </tr>
                    <tr>
                        <td><%=course2%></td>
                        <td><%=unit2%></td>
                    </tr>
                    <tr>
                        <td><%=course3%></td>
                        <td><%=unit3%></td>
                    </tr>
                    <tr>
                        <td><%=course4%></td>
                        <td><%=unit4%></td>
                    </tr>
                    <tr>
                        <td><%=course5%></td>
                        <td><%=unit5%></td>
                    </tr>
                    <tr>
                        <td><%=course6%></td>
                        <td><%=unit6%></td>
                    </tr>
                    <tr>
                        <td><%=course7%></td>
                        <td><%=unit7%></td>
                    </tr>
                    <tr>
                        <td><%=course8%></td>
                        <td><%=unit8%></td>
                    </tr>
                </tbody>
            </table>
            <div class="col-sm-3 col-md-3 col-xs-12 pull-right">
                <button type="submit" onclick="printDiv('register-content')" id="prtbtn" class="btn btn-success"><i class="glyphicon glyphicon-print"></i>Print Report</button>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<style>
    table{
        border: 0px solid #000000;
        width: 100%;
        margin: 10px;
    }
    th {
        border: 0px;
        width: 50%;
        height: 50px;
    }
    td {
        border: 0px;
        width: 50%;
        height: 50px;
    }
</style>
</body>
</html>

