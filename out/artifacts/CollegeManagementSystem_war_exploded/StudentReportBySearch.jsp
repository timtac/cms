<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/16/2015
  Time: 11:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">
                <%
                    String matricNo = (String) session.getAttribute("matricNo");
                    String level = (String) session.getAttribute("level");
                    String semester = (String) session.getAttribute("semester");
                    String contactSession = (String) session.getAttribute("contactSession");
                    String department = (String) session.getAttribute("department");
                    String course1 = (String) session.getAttribute("course1");
                    String grade1 = (String) session.getAttribute("grade1");
                    String course2 = (String) session.getAttribute("course2");
                    String grade2 = (String) session.getAttribute("grade2");
                    String course3= (String) session.getAttribute("course3");
                    String grade3 = (String) session.getAttribute("grade3");
                    String course4 = (String) session.getAttribute("course4");
                    String grade4 = (String) session.getAttribute("grade4");
                    String course5 = (String) session.getAttribute("course5");
                    String grade5 = (String) session.getAttribute("grade5");
                    String course6 = (String) session.getAttribute("course6");
                    String grade6 = (String) session.getAttribute("grade6");
                    String course7= (String) session.getAttribute("course7");
                    String grade7 = (String) session.getAttribute("grade7");
                    String course8 = (String) session.getAttribute("course8");
                    String grade8 = (String) session.getAttribute("grade8");

                %>

                <h3>Student Report</h3>

                <table>
                    <tr>
                        <th>MATRIC. NO.: <%=matricNo%></th>
                        <th>SESSION : <%=contactSession%></th>
                    </tr>
                    <tr>
                        <th>LEVEL : <%=level%></th>
                        <th>SEMESTER : <%=semester%></th>
                    </tr>
                    <tr>
                        <th>DEPARTMENT : <%=department%></th>
                        <th></th>
                    </tr>
                </table>

                <table>
                    <tr>
                        <th>COURSE<span class="caret"></span></th>
                        <th>GRADE<span class="caret"></span></th>
                    </tr>
                    <tr>
                        <th><%=course1%></th>
                        <th><%=grade1%></th>
                    </tr>
                    <tr>
                        <th><%=course2%></th>
                        <th><%=grade2%></th>
                    </tr>
                    <tr>
                        <th><%=course3%></th>
                        <th><%=grade3%></th>
                    </tr>
                    <tr>
                        <th><%=course4%></th>
                        <th><%=grade4%></th>
                    </tr>
                    <tr>
                        <th><%=course5%></th>
                        <th><%=grade5%></th>
                    </tr>
                    <tr>
                        <th><%=course6%></th>
                        <th><%=grade6%></th>
                    </tr>
                    <tr>
                        <th><%=course7%></th>
                        <th><%=grade7%></th>
                    </tr>
                    <tr>
                        <th><%=course8%></th>
                        <th><%=grade8%></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>

                </table>

                <div class="btn-group pull-right">
                    <div class="col-md-12 col-sm-6">
                        <button type="submit" onclick="printDiv('register-content')" id="prtbtn" class="btn btn-success"><i class="glyphicon glyphicon-print"></i>Print Report</button>
                        <input type="reset" value="Reset" id="rstbtn" class="btn btn-block btn-default">
                    </div>
                </div>

        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>

<style>
    table{
        border: 0px solid #000000;
        width: 100%;
        margin: 10px;
    }
    th {
        border: 0px;
        width: 50%;
        height: 50px;
    }
    td {
        border: 0px;
        width: 50%;
        height: 50px;
    }
</style>
</body>

</html>