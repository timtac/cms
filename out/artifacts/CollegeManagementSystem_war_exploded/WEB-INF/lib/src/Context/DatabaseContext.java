package Context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import Models.*;
import Helpers.DbConstant;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by user on 11/20/2015.
 */
public class DatabaseContext implements ServletContextListener {

    JdbcPooledConnectionSource con;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
            con.setUsername(DbConstant.DB_USER);
            con.setPassword(DbConstant.DB_PWD);

            TableUtils.createTableIfNotExists(con,Admin.class);
            TableUtils.createTableIfNotExists(con,StudentFeeForm.class);
            TableUtils.createTableIfNotExists(con,Employee.class);
            TableUtils.createTableIfNotExists(con,Student.class);
            TableUtils.createTableIfNotExists(con,StudentMark.class);
            TableUtils.createTableIfNotExists(con, CourseRegistration.class);
        }
        catch(Exception ex){
            System.out.println("An Error occured in databaseContext.class");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        try{
            con.close();
        }
        catch(Exception ex){

        }
    }
}
