package Controllers;

import Helpers.EmployeeDao;
import Models.Employee;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 11/21/2015.
 */
@WebServlet("/employee")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class EmployeeServlet extends HttpServlet {

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String post = request.getParameter("post");
        String hireDate = request.getParameter("hireDate");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

        HttpSession session = request.getSession();

        try{
            EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
            String nwpsd = crypto.encrypt(username,password);

            Models.Employee employee = new Models.Employee();
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setPost(post);
            employee.setHireDate(hireDate);
            employee.setUserId(username);
            employee.setPassword(nwpsd);
            employee.setAddress(address);
            employee.setAge(age);
            employee.setSex(sex);
            String filePath = savePath + File.separator + fileName;
            employee.setPhoto(filePath);

            Helpers.EmployeeDao employeeDao = new EmployeeDao(employee);
            employeeDao.createEmployee();

            session.setAttribute("firstname",firstName);
            session.setAttribute("last",lastName);
            session.setAttribute("post",post);
            session.setAttribute("address",address);
            session.setAttribute("age",age);
            session.setAttribute("sex",sex);
            session.setAttribute("image",filePath);
            response.sendRedirect("");
        }
        catch (Exception ex){
            response.sendRedirect("/enrolStudent.jsp");
        }
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String username = request.getParameter("");
        String password = request.getParameter("");

        HttpSession session = request.getSession();

            try{
                EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
                String psd = crypto.encrypt(username,password);

                Helpers.EmployeeDao employeeDao = new EmployeeDao();

                boolean result = employeeDao.authenticateEmployee(username,psd);

                if(result){
                    List<Employee> list = employeeDao.getEmployeeDetails(username);
                        for(int i=0; i < list.size(); i++){
                            Employee employee = list.get(i);

                            session.setAttribute("employee",employee);
                        }
                    response.setContentType("text/html;charset=UTF-8");
                    String url = "";
                    File file = new File(url);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    ServletOutputStream outputStream = response.getOutputStream();
                    response.setContentType("image/jpeg");
                    int i = 0;
                        while(i != -1){
                            i = fileInputStream.read();
                            outputStream.write(i);
                        }
                    fileInputStream.close();
                    response.sendRedirect("");

                }
                else{

                }

            }
            catch(Exception ex){

            }

    }

}
