package Controllers;

import Helpers.StudentDao;
import Models.Student;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by user on 11/21/2015.
 */
@WebServlet("/student")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class StudentServlet extends HttpServlet {

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String course = request.getParameter("course");
        String department = request.getParameter("department");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        String matricNo = request.getParameter("matricNo");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

            HttpSession session = request.getSession();
        try{
            Student student = new Student();
            student.setFirstName(firstName);
            student.setLastName(lastName);
            student.setCourse(course);
            student.setDepartment(department);
            student.setMatricNo(matricNo);
            student.setSex(sex);
            student.setAddress(address);
            student.setAge(age);
            String filePath = savePath + File.separator + fileName;
            student.setPhoto(filePath);

            Helpers.StudentDao  studentDao = new StudentDao(student);
            studentDao.createStudent();

            session.setAttribute("","");
            session.setAttribute("firstname",firstName);
            session.setAttribute("lastname",lastName);
            session.setAttribute("course",course);
            session.setAttribute("department",department);
            session.setAttribute("matricNo",matricNo);
            session.setAttribute("sex",sex);
            session.setAttribute("address",address);
            session.setAttribute("age",age);
            session.setAttribute("image",filePath);
        }catch(Exception ex){

        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        String matricNo = request.getParameter("");

        try{
            StudentDao studentDao = new StudentDao();
            List<Student> list = studentDao.getStudent(matricNo);
        }
        catch(Exception ex){

        }
    }
}
