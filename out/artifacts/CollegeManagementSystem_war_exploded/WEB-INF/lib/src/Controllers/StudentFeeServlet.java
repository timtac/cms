package Controllers;

import Helpers.StudentFeeDao;
import Models.StudentFeeForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by user on 12/4/2015.
 */
@WebServlet("/studentFee")
public class StudentFeeServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String fullName = request.getParameter("name");
        String department = request.getParameter("department");
        String level = request.getParameter("level");
        String session = request.getParameter("session");
        String matricNo = request.getParameter("matricNo");
        String amountInWords = request.getParameter("amountWords");
        String amountInFigures = request.getParameter("amountFigure");
        String paymentFor = request.getParameter("paymentFor");
        String recievedBy = request.getParameter("recievedBy");

        HttpSession httpSession = request.getSession();

        try{
            StudentFeeForm feeForm = new StudentFeeForm();
            feeForm.setStudentName(fullName);
            feeForm.setDepartment(department);
            feeForm.setLevel(level);
            feeForm.setContactSession(session);
            feeForm.setMatricNo(matricNo);
            feeForm.setAmountInWords(amountInWords);
            feeForm.setAmountInFigure(amountInFigures);
            feeForm.setPaymentFor(paymentFor);
            feeForm.setRecievedBy(recievedBy);

            StudentFeeDao feeDao = new StudentFeeDao(feeForm);
            feeDao.createFee();

            httpSession.setAttribute("","");
            httpSession.setAttribute("fullName",fullName);
            httpSession.setAttribute("department",department);
            httpSession.setAttribute("level",level);
            httpSession.setAttribute("session",session);
            httpSession.setAttribute("matricNo",matricNo);
            httpSession.setAttribute("amountWord",amountInWords);
            httpSession.setAttribute("amountFigure",amountInFigures);
            httpSession.setAttribute("paymentFor",paymentFor);
            httpSession.setAttribute("recieved",recievedBy);

        }
        catch(Exception ex){

        }
    }
}
