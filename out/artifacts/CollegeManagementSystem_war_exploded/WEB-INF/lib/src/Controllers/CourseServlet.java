package Controllers;

import Helpers.CourseDao;
import Helpers.StudentDao;
import Models.CourseRegistration;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by user on 11/23/2015.
 */
@WebServlet("/registerCourse")

public class CourseServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {

        String studentmatric = request.getParameter("matricNo");
        String level = request.getParameter("level");
        String semester = request.getParameter("semester");
        String contactSession = request.getParameter("session");
        String department = request.getParameter("department");
        String course1 = request.getParameter("course1");
        String unit1 = request.getParameter("unit1");
        String course2 = request.getParameter("course2");
        String unit2 = request.getParameter("unit2");
        String course3 = request.getParameter("course3");
        String unit3 = request.getParameter("unit3");
        String course4 = request.getParameter("course4");
        String unit4 = request.getParameter("unit4");
        String course5 = request.getParameter("course5");
        String unit5 = request.getParameter("unit5");
        String course6 = request.getParameter("course6");
        String unit6 = request.getParameter("unit6");
        String course7 = request.getParameter("course7");
        String unit7 = request.getParameter("unit7");
        String course8 = request.getParameter("course8");
        String unit8 = request.getParameter("unit8");

        HttpSession session = request.getSession();
        try{
            Models.Student student = new Models.Student();
            student.setMatricNo(studentmatric);
            StudentDao studentDao = new StudentDao(student);
            studentDao.refreshStudent();

             //save the student object into the database at this point and let every other thing like that
            CourseRegistration course = new CourseRegistration();
            course.setStudent_matric_no(student);
            course.setLevel(level);
            course.setSemester(semester);
            course.setContactSession(contactSession);
            course.setDepartment(department);
            course.setCourse1(course1);
            course.setUnit1(unit1);
            course.setCourse2(course2);
            course.setUnit2(unit2);
            course.setCourse3(course3);
            course.setUnit3(unit3);
            course.setCourse4(course4);
            course.setUnit4(unit4);
            course.setCourse5(course5);
            course.setUnit5(unit5);
            course.setCourse6(course6);
            course.setUnit6(unit6);
            course.setCourse7(course7);
            course.setUnit7(unit7);
            course.setCourse8(course8);
            course.setUnit8(unit8);
            //needs to set total unit course

            CourseDao courseDao = new CourseDao(course);
            courseDao.registerCourse();

            session.setAttribute("","");
            session.setAttribute("matricNo",studentmatric);
            session.setAttribute("level",level);
            session.setAttribute("semester",semester);
            session.setAttribute("session",contactSession);
            session.setAttribute("department",department);
            session.setAttribute("course1",course1);
            session.setAttribute("unit1",unit1);
            session.setAttribute("course2",course2);
            session.setAttribute("unit2",unit2);
            session.setAttribute("course3",course3);
            session.setAttribute("unit3",unit3);
            session.setAttribute("course4",course4);
            session.setAttribute("unit4",unit4);
            session.setAttribute("course5",course5);
            session.setAttribute("unit5",unit5);
            session.setAttribute("course6",course6);
            session.setAttribute("unit6",unit6);
            session.setAttribute("course7",course7);
            session.setAttribute("unit7",unit7);
            session.setAttribute("course8",course8);
            session.setAttribute("unit8",unit8);
        }
        catch (Exception ex){

        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String matricNo = request.getParameter("");
        String department = request.getParameter("");
        String level = request.getParameter("");
        String semester = request.getParameter("");

        try {
            CourseDao courseDao = new CourseDao();
            courseDao.queryRegisteredCourse(matricNo,level);

        }
        catch(Exception ex){

        }
    }
}

