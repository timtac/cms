package Controllers;

import Helpers.StudentDao;
import Models.Student;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 12/6/2015.
 */
public class AllStudentServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            StudentDao studentDao = new StudentDao();

            List<Student> list = studentDao.allStudent();
            JsonArray jsonArray = new JsonArray();

            for(int count = 0;count < list.size();count ++){
                Student student = list.get(count);

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("firstname",student.getFirstName());
                jsonObject.addProperty("lastname",student.getLastName());
                jsonObject.addProperty("course",student.getCourse());
                jsonObject.addProperty("department",student.getDepartment());
                jsonObject.addProperty("matricNo", student.getMatricNo());
                jsonObject.addProperty("age",student.getAge());
                jsonObject.addProperty("sex", student.getSex());
                jsonObject.addProperty("address", student.getAddress());
                jsonObject.addProperty("photo", student.getPhoto());

                jsonArray.add(jsonObject);
            }

            out.println(jsonArray.toString());


        }
        catch(Exception ex){

        }
    }
}
