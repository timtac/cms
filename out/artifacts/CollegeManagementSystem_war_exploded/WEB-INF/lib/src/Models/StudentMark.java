package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 11/22/2015.
 */
@DatabaseTable(tableName = "student_mark")
public class StudentMark {
    @DatabaseField(foreign = true,columnName = "student_matric_no",canBeNull = false)
    private Student student_matric_no;
    @DatabaseField
    private String level;
    @DatabaseField
    private String semester;
    @DatabaseField
    private String contactSession;
    @DatabaseField
    private String course1;
    @DatabaseField
    private String score1;
    @DatabaseField
    private String grade1;
    @DatabaseField
    private String course2;
    @DatabaseField
    private String score2;
    @DatabaseField
    private String grade2;
    @DatabaseField
    private String course3;
    @DatabaseField
    private String score3;
    @DatabaseField
    private String grade3;
    @DatabaseField
    private String course4;
    @DatabaseField
    private String score4;
    @DatabaseField
    private String grade4;
    @DatabaseField
    private String course5;
    @DatabaseField
    private String score5;
    @DatabaseField
    private String grade5;
    @DatabaseField
    private String course6;
    @DatabaseField
    private String score6;
    @DatabaseField
    private String grade6;
    @DatabaseField
    private String course7;
    @DatabaseField
    private String score7;
    @DatabaseField
    private String grade7;
    @DatabaseField
    private String course8;
    @DatabaseField
    private String score8;
    @DatabaseField
    private String grade8;

    public StudentMark(){

    }

    public Student getStudent_matric_no() {
        return student_matric_no;
    }

    public void setStudent_matric_no(Student student_matric_no) {
        this.student_matric_no = student_matric_no;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getContactSession() {
        return contactSession;
    }

    public void setContactSession(String contactSession) {
        this.contactSession = contactSession;
    }

    public String getCourse1() {
        return course1;
    }

    public void setCourse1(String course1) {
        this.course1 = course1;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getCourse2() {
        return course2;
    }

    public void setCourse2(String course2) {
        this.course2 = course2;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getCourse3() {
        return course3;
    }

    public void setCourse3(String course3) {
        this.course3 = course3;
    }

    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {
        this.score3 = score3;
    }

    public String getCourse4() {
        return course4;
    }

    public void setCourse4(String course4) {
        this.course4 = course4;
    }

    public String getScore4() {
        return score4;
    }

    public void setScore4(String score4) {
        this.score4 = score4;
    }

    public String getCourse5() {
        return course5;
    }

    public void setCourse5(String course5) {
        this.course5 = course5;
    }

    public String getScore5() {
        return score5;
    }

    public void setScore5(String score5) {
        this.score5 = score5;
    }

    public String getCourse6() {
        return course6;
    }

    public void setCourse6(String course6) {
        this.course6 = course6;
    }

    public String getScore6() {
        return score6;
    }

    public void setScore6(String score6) {
        this.score6 = score6;
    }

    public String getCourse7() {
        return course7;
    }

    public void setCourse7(String course7) {
        this.course7 = course7;
    }

    public String getScore7() {
        return score7;
    }

    public void setScore7(String score7) {
        this.score7 = score7;
    }

    public String getCourse8() {
        return course8;
    }

    public void setCourse8(String course8) {
        this.course8 = course8;
    }

    public String getScore8() {
        return score8;
    }

    public void setScore8(String score8) {
        this.score8 = score8;
    }

    public String getGrade1() {
        return grade1;
    }

    public void setGrade1(String grade1) {
        this.grade1 = grade1;
    }

    public String getGrade2() {
        return grade2;
    }

    public void setGrade2(String grade2) {
        this.grade2 = grade2;
    }

    public String getGrade3() {
        return grade3;
    }

    public void setGrade3(String grade3) {
        this.grade3 = grade3;
    }

    public String getGrade4() {
        return grade4;
    }

    public void setGrade4(String grade4) {
        this.grade4 = grade4;
    }

    public String getGrade5() {
        return grade5;
    }

    public void setGrade5(String grade5) {
        this.grade5 = grade5;
    }

    public String getGrade6() {
        return grade6;
    }

    public void setGrade6(String grade6) {
        this.grade6 = grade6;
    }

    public String getGrade7() {
        return grade7;
    }

    public void setGrade7(String grade7) {
        this.grade7 = grade7;
    }

    public String getGrade8() {
        return grade8;
    }

    public void setGrade8(String grade8) {
        this.grade8 = grade8;
    }
}
