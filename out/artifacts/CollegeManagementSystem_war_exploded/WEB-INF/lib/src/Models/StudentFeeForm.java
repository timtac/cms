package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by user on 11/22/2015.
 */
@DatabaseTable(tableName = "student_fee_form")
public class StudentFeeForm {
    @DatabaseField(generatedId = true)
    private int slipNo;
    @DatabaseField
    private String studentName;
    @DatabaseField
    private String department;
    @DatabaseField
    private String contactSession;
    @DatabaseField
    private String level;
    @DatabaseField
    private String matricNo;
    @DatabaseField
    private String amountInWords;
    @DatabaseField
    private String amountInFigure;
    @DatabaseField
    private String paymentFor;
    @DatabaseField
    private String recievedBy;

    public StudentFeeForm(){

    }

    public String getStudentName() {
        return studentName;
    }

    public int getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(int slipNo) {
        this.slipNo = slipNo;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContactSession() {
        return contactSession;
    }

    public void setContactSession(String contactSession) {
        this.contactSession = contactSession;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMatricNo() {
        return matricNo;
    }

    public void setMatricNo(String matricNo) {
        this.matricNo = matricNo;
    }

    public String getAmountInWords() {
        return amountInWords;
    }

    public void setAmountInWords(String amountInWords) {
        this.amountInWords = amountInWords;
    }

    public String getAmountInFigure() {
        return amountInFigure;
    }

    public void setAmountInFigure(String amountInFigure) {
        this.amountInFigure = amountInFigure;
    }

    public String getPaymentFor() {
        return paymentFor;
    }

    public void setPaymentFor(String paymentFor) {
        this.paymentFor = paymentFor;
    }

    public String getRecievedBy() {
        return recievedBy;
    }

    public void setRecievedBy(String recievedBy) {
        this.recievedBy = recievedBy;
    }
}
