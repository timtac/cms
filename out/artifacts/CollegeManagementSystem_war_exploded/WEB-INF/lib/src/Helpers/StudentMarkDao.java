package Helpers;

import Helpers.DbConstant;
import Models.StudentMark;
import Models.Student;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by user on 11/26/2015.
 */
public class StudentMarkDao {
    JdbcPooledConnectionSource con;
    StudentMark mark;

    public StudentMarkDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public StudentMarkDao(StudentMark mark) throws Exception{
        this.mark = mark;

        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void setStudentScores() throws Exception{
        Dao<StudentMark, String> studentMarksDao = DaoManager.createDao(con,StudentMark.class);
        studentMarksDao.create(mark);
    }
    //get student scores by matric no and by the selected semester
    /*public List<StudentMark> getStudentScore(Student matricNo,String semester) throws Exception{
        Dao<StudentMark, String > studentMarkDao = DaoManager.createDao(con,StudentMark.class);
        QueryBuilder<StudentMark,String> queryBuilder = studentMarkDao.queryBuilder();
        Where<StudentMark,String> where = queryBuilder.where();
        where.eq(mark.getStudent_matric_no(), matricNo);
        PreparedQuery<StudentMark> preparedQuery =
    }*/
}
