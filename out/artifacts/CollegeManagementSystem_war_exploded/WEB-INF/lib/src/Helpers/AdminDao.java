package Helpers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import Models.Admin;

import java.util.List;

/**
 * Created by user on 11/20/2015.
 */

public class AdminDao {
    JdbcPooledConnectionSource con;
    Admin admin;

    public AdminDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public AdminDao(Admin admin) throws Exception{
        this.admin = admin;

        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void createAdmin() throws Exception{
        Dao<Admin,String> adminDao = DaoManager.createDao(con,Admin.class);
        adminDao.createIfNotExists(admin);
    }

    public boolean authenticateAdmin(String username,String password) throws Exception {
        Dao<Admin,String> adminDao = DaoManager.createDao(con,Admin.class);

        List<Admin> list = adminDao.queryForAll();
        for(int i = 0; i < list.size(); i++){
                admin = list.get(1);
            if(admin.getUsername().equals(username) && admin.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }

    public Admin getAdmin(int id) throws Exception{
        Dao<Admin,String> adminDao =  DaoManager.createDao(con,Admin.class);
        return adminDao.queryForId(String.valueOf(id));
    }

}
