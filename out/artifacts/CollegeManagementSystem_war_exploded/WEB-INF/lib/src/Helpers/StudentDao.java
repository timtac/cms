package Helpers;

import Models.Student;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by user on 11/21/2015.
 */
public class StudentDao {
    JdbcPooledConnectionSource con;
    Models.Student student;

    public StudentDao() throws Exception {
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public StudentDao(Models.Student student) throws Exception {
        this.student = student;

        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void createStudent()throws Exception{
        Dao<Student, String> studentDao = DaoManager.createDao(con,Student.class);
        studentDao.create(student);
    }

    public void refreshStudent() throws Exception{
        Dao<Student, String> studentDao = DaoManager.createDao(con,Student.class);
        studentDao.refresh(student);
    }


    public List<Student> allStudent() throws Exception {
        Dao<Student,String> studentDao = DaoManager.createDao(con,Student.class);
        return studentDao.queryForAll();
    }

    public List<Student> getStudent(String matricNo) throws Exception{
        Dao<Student,String> studentDao = DaoManager.createDao(con,Student.class);
        QueryBuilder queryBuilder = studentDao.queryBuilder();
        Where<Student,String> where = queryBuilder.where();
        where.eq(student.getMatricNo(), matricNo);
        PreparedQuery<Student> preparedQuery = queryBuilder.prepare();

        return studentDao.query(preparedQuery);
    }
}
