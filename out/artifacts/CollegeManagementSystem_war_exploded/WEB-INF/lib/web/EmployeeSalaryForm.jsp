<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/
		    ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <div><center>
        <ul>
            <li><a href="#">Home</a></li> |
            <li><a href="#">Login</a></li> |
            <li><a href="#">Page</a></li> |
            <li><a href="#">Hover</a></li>
            <li></li>
            <li></li>
            <li></li>

        </ul></center>
    </div>

    <div id="main-content">
        <div id="register-content">

            <form action="" method="" class="form-horizontal">

                <h3>EMPLOYEE'S PAYROLL</h3>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course1" id="course1" placeholder="Course" /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Post:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course2" id="post"  /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Pay Per. Day:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course3" id="pay"  /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Days:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course4" id="days"  /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Bonus:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course5" id="bonus"  /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Total:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course6" id="total" readonly/> <span class="required">*</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="submit" value="Calculate & Generate" id="submit" class="btn btn-block btn-success" onclick="calculatePayment()">
                        <input type="reset" value="Clear" class="btn btn-block btn-default">
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
</body>
</html>

