<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/
		    ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <div><center>
        <ul>
            <li><a href="#">Home</a></li> |
            <li><a href="#">Login</a></li> |
            <li><a href="#">Page</a></li> |
            <li><a href="#">Hover</a></li>
            <li></li>
            <li></li>
            <li></li>

        </ul></center>
    </div>

    <div id="main-content">
        <div id="register-content">

            <form action="/mark" method="post" class="form-horizontal">

                <h3>REGISTER COURSE</h3>

                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Matric. No.:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="matricNo" id="matric" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Level:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="level" id="level" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Department:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="department" id="department" placeholder="" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm- col-xs-12">Semester:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="semester" id="semester" placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Session:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="session" id="session" placeholder="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course1" id="course1" placeholder="Course" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score1" id="score1"  onblur="genarateGrades(this.value)"/> <input id="required1" name="grade1" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course2" id="course2" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score2" id="score2" onblur="genarateGrades1(this.value)" /><input id="required2" name="grade2" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course3" id="course3" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score3" id="score3" onblur="genarateGrades2(this.value)" /><input id="required3" name="grade3" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course4" id="course4" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score4" id="score4" onblur="genarateGrades3(this.value)" /><input id="required4" name="grade4" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course5" id="course5" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score5" id="score5" onblur="genarateGrades4(this.value)" /><input id="required5" name="grade5" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course6" id="course6" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score6" id="score6" onblur="genarateGrades5(this.value)" /><input id="required6" name="grade6" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course7" id="course7" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score7" id="score7" onblur="genarateGrades6(this.value)" /><input id="required7" name="grade7" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course8" id="course8" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="score8" id="score8" onblur="genarateGrades7(this.value)" /><input id="required8" name="grade8" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="btn-group pull-right">
                    <div class="col-md-12 col-sm-6">
                        <input type="submit" value="Submit & Generate Report" id="submit" class="btn btn-block btn-success">
                        <input type="reset" value="Reset" class="btn btn-block btn-default">
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
</body>
</html>
