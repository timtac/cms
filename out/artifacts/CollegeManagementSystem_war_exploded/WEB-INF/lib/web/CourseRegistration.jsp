<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11/26/2015
  Time: 11:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/
		    ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <div><center>
        <ul>
            <li><a href="#">Home</a></li> |
            <li><a href="#">Login</a></li> |
            <li><a href="#">Page</a></li> |
            <li><a href="#">Hover</a></li>
            <li></li>
            <li></li>
            <li></li>

        </ul></center>
    </div>

    <div id="main-content">
        <div id="register-content">

            <form action="/registerCourse" method="post" class="form-horizontal">

                <h3>REGISTER COURSE</h3>

                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Matric. No.:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="matricNo" id="matric" placeholder="Joe" /> <span class="required">*</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Level:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <select name="level" class="col-md-6 col-sm-6 col-xs-12" size="1">
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="300">300</option>
                                    <option value="400">400</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Department:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="department" id="department" placeholder="Campbell" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm- col-xs-12">Semester:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <select name="semester" class="col-md-12 col-sm-12 col-xs-12" size="1">
                                    <option value="1st">First Semester</option>
                                    <option value="2nd">Second Semester</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Session:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="session" id="session" placeholder="Campbell" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course1" id="course1"  />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit1" id="unit1" placeholder=" Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course2" id="course2"  />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit2" id="unit2" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course3" id="course3" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit3" id="unit3" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course4" id="course4" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit4" id="unit4" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course5" id="course5" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit5" id="unit5" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course6" id="course6" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit6" id="unit6" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course7" id="course7"  />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit7" id="unit7" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course8" id="course8" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Unit:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="unit8" id="unit8" placeholder="Unit" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="submit" value="Submit" id="submit" class="btn btn-block btn-success">
                        <input type="reset" value="Reset" class="btn btn-block btn-default">
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
</body>
</html>
