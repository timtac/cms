<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11/26/2015
  Time: 11:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
    <head><title>College Management System</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/
		    ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="custom/custom.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>

        <link rel="stylesheet" type="text/css" href="custom/custom.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>
        <div>

            <header><img src="custom/img/schoolsys-Banner.png"></header>

            <div><center>
                <ul>
                    <li><a href="#">Home</a></li> |
                    <li><a href="#">Login</a></li> |
                    <li><a href="#">Page</a></li> |
                    <li><a href="#">Hover</a></li>
                    <li></li>
                    <li></li>
                    <li></li>

                </ul></center>
            </div>

                <div id="main-content">
                    <div id="register-content">

                        <form action="/employee" method="POST" enctype="multipart/form-data" class="form-horizontal">

                            <h3>REGISTER EMPLOYEE</h3>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm- col-xs-12">Firstname:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="firstname" id="firstname" placeholder="Joe" /> <span class="required">*</span>
                            </div>
                        </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Lastname:</label>
                                <div class="col-sm-6 col-md-6 col-xs-12">
                                    <input type="text" name="lastname" id="lastname" placeholder="Campbell" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12"> Post:</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" name="post" id="post" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12">Hire Date:</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="date" name="hireDate" id="hireDate" max="2020-12-31" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12"> Username:</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" name="username" id="username" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12"> Password:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" name="password" id="psd" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12"> Age:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" name="age" id="age" max="2015-12-31" />
                                </div>
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12"> Sex:</label>
                                <input type="radio" name="sex" id="sex" placeholder="" />Male
                                <input type="radio" name="sex" id="" placeholder="" />Female
                            </div>
                            <div class="form-group">
                            <label class="form-control col-sm-3 col-md-3 col-xs-12"> Address: </label>
                            <textarea size="30" placeholder="Home Address" name="address" ></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-md-3 col-xs-12">Image:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="file" />
                                </div>
                            </div>
                            <div class="btn-group pull-right">
                                <input type="submit" value="Submit" id="submit" class="btn btn-block btn-success">
                                <input type="reset" value="Reset" class="btn btn-block btn-default">
                            </div>

                         </form>
                    </div>
                </div>
            <div class="row">


                <div class="col-sm-4">
                    <h6>Social Platforms</h6>
                    <p><img src="custom/img/social-share/google-plus.png"></p>
                    <p><img src="custom/img/social-share/my-space.png"></p>
                    <p><img src="custom/img/social-share/picasa.png"></p>
                    <p><img src="custom/img/social-share/pinterest.png"></p>
                    <p><img src="custom/img/social-share/rss.png"></p>
                    <p><img src="custom/img/social-share/linkedin.png"></p>
                </div>



                <div  class="col-sm-4">
                    <h6>Social Platforms</h6>
                    <p><img src="custom/img/social-share/google-plus.png"></p>
                    <p><img src="custom/img/social-share/my-space.png"></p>
                    <p><img src="custom/img/social-share/picasa.png"></p>
                    <p><img src="custom/img/social-share/pinterest.png"></p>
                    <p><img src="custom/img/social-share/rss.png"></p>
                    <p><img src="custom/img/social-share/linkedin.png"></p>
                </div>


                <div  class="col-sm-4">
                    <h6>Social Platforms</h6>
                    <p><img src="custom/img/social-share/google-plus.png"></p>
                    <p><img src="custom/img/social-share/my-space.png"></p>
                    <p><img src="custom/img/social-share/picasa.png"></p>
                    <p><img src="custom/img/social-share/pinterest.png"></p>
                    <p><img src="custom/img/social-share/rss.png"></p>
                    <p><img src="custom/img/social-share/linkedin.png"></p>
                </div>

            </div>
                <footer>
                    <center>Copyright 2015&copy</center>
                </footer>

        </div>
    </body>
</html>