<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>
    <%
        String username = (String) session.getAttribute("username");
    %>
    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="AdminHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="registerEmployees.jsp">Register Employee</a></li>
                            <li><a href="EmployeeIDForm.jsp">Employee ID</a></li>

                        </ul>
                    </li>
                    <li><a href="EmployeeSalaryForm.jsp">Salary</a></li>
                    <li><a href="AllEmployee.jsp">All Employee(s)</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchEmployee.jsp"> Employee(s)</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">

            <form action="EmployeeSalaryReport.jsp" onsubmit="return employeeSalary()" class="form-horizontal">

                <h3>EMPLOYEE'S PAYROLL</h3>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="name" id="name"  />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Post:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="post" id="post"  />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Pay Per. Day:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="number" name="pay" id="pay"  />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Days:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="number" name="days" id="days"  />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Bonus:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="number" name="bonus" id="bonus"  />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Total:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="total" id="total" />
                    </div>
                </div>

                <div class="form-group pull-right">
                    <div class="">
                        <button class="btn btn-primary" type="button" onclick="calculatePayment()" >Calculate</button>
                        <input type="reset" value="Clear" class="btn btn-block btn-default">
                        <input type="submit" value="Generate Slip" id="submit" class="btn btn-block btn-success" >
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
</body>
</html>

