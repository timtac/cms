<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/22/2015
  Time: 3:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>



    <div id="main-content">
        <div id="content">
            <h3>Login</h3>
            <form action="" method="post" onsubmit="return loginValidate()" class="form-horizontal" id="loginForm">
                <div class="form-group">
                    <div class="col-sm-1">
                        <input type="text" name="username" id="username" placeholder="username" /><span class="label label-danger">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-1">
                        <input type="password" name="password" id="psd" placeholder="password" /><span class="label label-danger">*</span>
                    </div>
                </div>

                <center>
                    <select class="" id="role" size="1" onchange="getRole()">
                        <option value="">Choose Role</option>
                        <option value="1">Admin</option>
                        <option value="2">Employee</option>
                    </select>

                    <input type="submit" value="Submit" id="submit" class="btn btn-success"> <br />
                    <a href="index.jsp">Register Here</a>
                </center>
            </form>
        </div>
    </div>


    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer >
        <div class="footee">
            <center><span class="copy">Copyright &copy 2015</span></center>
        </div>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#username').focus()
    })

</script>
</body>
</html>
