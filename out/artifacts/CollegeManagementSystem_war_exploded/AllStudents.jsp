<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/22/2015
  Time: 3:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">
            <center><h3>All Students</h3></center>
            <table>
                <tr>
                    <th>S/N</th>
                    <th>Matric No.</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Faculty</th>
                    <th>Department</th>
                    <th>D.O.B</th>
                    <th>Sex</th>
                    <th>Address</th>
                </tr>
            </table>

        </div>
    </div>


    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer >
        <div class="footee">
            <center><span class="copy">Copyright &copy 2015</span></center>
        </div>
    </footer>

</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('#username').focus()
    })


    $(document).ready(function(){
        var i =0
        $.ajax({
            type:'GET',
            url:'/allStudents',
            success:function (response){

                var jsonResponse = JSON.parse(response);

                while(jsonResponse.length > i){
                    var id = jsonResponse[i].sn;
                    var firstname = jsonResponse[i].firstname;
                    var lastname = jsonResponse[i].lastname;
                    var faculty = jsonResponse[i].faculty;
                    var department = jsonResponse[i].department;
                    var matricNo = jsonResponse[i].matricNo;
                    var age = jsonResponse[i].age;
                    var sex = jsonResponse[i].sex;
                    var address = jsonResponse[i].address;


                    $('div#register-content table').append('<tr><td>'+id+'</td><td>'+matricNo+'</td><td>'+firstname+'</td><td>'+lastname+'</td><td>'+faculty+'</td><td>'+department+'</td><td>'+age+'</td><td>'+sex+'</td><td>'+address+'</td>')

                    i = i + 1;
                }


//                .appendTo('$div')
  //              $('<br /><br /><br /><table> <tr> <th>COURSE</th> <th>GRADE</th> </tr> <tr> <th>'+course1+'</th> <th>'+grade1+'</th> </tr><tr> <th>'+course2+'</th> <th>'+grade2+'</th> </tr> <tr> <th>'+course3+'</th> <th>'+grade3+'</th> </tr> <tr> <th>'+course4+'</th> <th>'+grade4+'</th> </tr> <tr> <th>'+course5+'</th> <th>'+grade5+'</th> </tr> <tr> <th>'+course6+'</th> <th>'+grade6+'</th> </tr> <tr> <th>'+course7+'</th> <th>'+grade7+'</th> </tr> <tr> <th>'+course8+'</th> <th>'+grade8+'</th> </tr> </table>')
            },
            error: function (response){

            }
        })
    })

</script>
<style>
    table{
        width: 100%;
        border: 1px solid greenyellow;
    }
    tr{
        border-bottom: 1px dotted lemonchiffon;
    }
</style>
</body>
</html>
