<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(session != null){
        String state = (String) session.getAttribute("fee");
        if(state == null){
            response.sendRedirect("/studentFeeForm.jsp");
        }
    }
    else{
        response.sendRedirect("/studentFeeForm.jsp");
    }
%>

<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
        <%
            String fullname = (String) session.getAttribute("fullName");
            String department = (String) session.getAttribute("department");
            String level = (String) session.getAttribute("level");
            String contactSession = (String) session.getAttribute("session");
            String matricNo = (String) session.getAttribute("MatricNo");
            String amountWord = (String) session.getAttribute("amountWord");
            String amountFigure = (String) session.getAttribute("amountFigure");
            String paymentFor = (String) session.getAttribute("paymentFor");
            String recieved = (String) session.getAttribute("recieved");
        %>

    <div id="main-content">
        <div id="register-content">
            <center><h3>PAYMENT SLIP</h3></center>
            <table>
                <tr>
                    <th>FULL NAME:</th>
                    <td><%=fullname%></td>
                </tr>
                <tr>
                    <th>MATRIC. NO:</th>
                    <td><%=matricNo%></td>
                </tr>
                <tr>
                    <th>CONTACT SESSION:</th>
                    <td><%=contactSession%></td>
                </tr>
                <tr>
                    <th>LEVEL:</th>
                    <td><%=level%></td>
                </tr>
                <tr>
                    <th>DEPARTMENT:</th>
                    <td><%=department%></td>
                </tr>
                <tr>
                    <th>AMOUNT IN WORDS:</th>
                    <td><%=amountWord%></td>
                </tr>
                <tr>
                    <th>AMOUNT IN FIGURE:</th>
                    <td><%=amountFigure%></td>
                </tr>
                <tr>
                    <th>PAYMENT FOR:</th>
                    <td><%=paymentFor%></td>
                </tr>
                <tr>
                    <th>RECEIVED BY:</th>
                    <td><%=recieved%></td>
                </tr>
            </table>


            <table>

            </table>
            <div class="col-sm-3 col-md-3 col-xs-12 pull-right">
                <button type="submit" onclick="printDiv('register-content')" id="prtbtn" class="btn btn-success"><i class="glyphicon glyphicon-print"></i>Print Report</button>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>

<style>
    table, th, td {
        padding: 5px;
        border: 1px solid black;
    }

</style>
</body>
</html>

                                                                                                                                                                                                                                                                                                          