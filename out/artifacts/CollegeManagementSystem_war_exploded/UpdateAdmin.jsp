<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 1/2/2016
  Time: 9:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>

<body>
<div>
    <%
        String username = (String) session.getAttribute("username");
    %>
    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active" aria-disabled="true"><a href="#">Home</a></li>
                    <li class="dropdown" aria-disabled="true">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li aria-disabled="true"><a href="#">Register Employee</a></li>
                            <li aria-disabled="true"><a href="#">Employee ID</a></li>

                        </ul>
                    </li>
                    <li aria-disabled="true"><a href="#">Salary</a></li>
                    <li aria-disabled="true"><a href="#">All Employee(s)</a></li>
                    <li class="dropdown" aria-disabled="true">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"> Employee(s)</a></li>
                            <li><a href="#"> Student(s)</a></li>
                            <li><a href="#">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" aria-disabled="true">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">
            <h3>REGISTER ADMIN</h3>
            <form action="/admin" onsubmit="return validateAdmin()" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="">


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm- col-xs-12">Firstname:</label>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <input type="text" name="firstname" id="firstname" placeholder="Joe" /> <span class="required">*</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Lastname:</label>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <input type="text" name="lastname" id="lastname" placeholder="Campbell" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Post:</label>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <input type="text" name="post" id="post" placeholder="Campbell" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Username:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="username" id="username" placeholder="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Password: </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="password" id="psd" placeholder="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Age:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="age" id="age" placeholder="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Sex: </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="radio" name="sex" id="sex" value="male" />Male
                        <input type="radio" name="sex" id="" value="female" />Female
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12"> Address:</label>
                    <textarea size="30" placeholder="Home Address" name="address" id="address" style="width: 277px; height: 88px"></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Image:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="submit" value="Submit" id="submit" class="btn btn-success ">
                        <input type="reset" value="Clear" class="btn btn-default ">
                    </div>
                </div>
                Note:<em>You must have an Admin role to register.</em>
            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#firstname').focus()
    })
</script>
</body>
</html>

