<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/7/2015
  Time: 12:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null){
        String status = (String) session.getAttribute("");
        if(status == null){

        }
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    <%
        String username = (String) session.getAttribute("username");
    %>
<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="AdminHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="registerEmployees.jsp">Register Employee</a></li>
                            <li><a href="EmployeeIDForm.jsp">Employee ID</a></li>

                        </ul>
                    </li>
                    <li><a href="EmployeeSalaryForm.jsp">Salary</a></li>
                    <li><a href="AllEmployee.jsp">All Employee(s)</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchEmployee.jsp"> Employee(s)</a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">

            <form action="EmployeeIDReport.jsp" onsubmit="return validateEmpID()" method="post" class="form-horizontal">
                <h3>Employee ID CARD</h3>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Full Name:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="fullname" id="name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Post Held:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="postHeld" id="post" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Employee ID No.:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="empIdNo" id="empId" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Next Of Kin:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="nextOfKin" id="kins" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Image:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="image" id="file" />
                    </div>
                </div>
                <div class="btn-group pull-right">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="submit" value="Submit & Generate" id="submit" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-default ">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
    <script>
        $(document).ready(function(){
            $('#name').focus()
        })
    </script>
</body>
</html>