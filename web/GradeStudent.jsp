<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div class="pull-right ">
            <select class="col-lg-12">
                <option value="">Search</option>
                <option value="">Student Details</option>
                <option value="">Student Report</option>
            </select>

        </div>
        <div id="register-content">

            <form action="/mark" onsubmit="return gradeStudent()" method="post" class="form-horizontal">

                <h3>REGISTER COURSE</h3>

                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Matric. No.:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="matricNo" id="matric" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Level:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <select name="level" class="col-md-6 col-sm-6 col-xs-12" size="1" id="level">
                                    <option value="">Choose Level</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="300">300</option>
                                    <option value="400">400</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Department:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="department" id="department" placeholder="" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm- col-xs-12">Semester:</label>
                            <div class="col-lg-12 col-md-6 col-xs-12">
                                <select name="semester" class="col-lg-12" size="1" id="semester">
                                    <option value="">Choose Semester</option>
                                    <option value="1st">First Semester</option>
                                    <option value="2nd">Second Semester</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12">Session:</label>
                            <div class="col-sm-6 col-md-6 col-xs-12">
                                <input type="text" name="session" id="session" placeholder="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course1" id="course1" placeholder="Course" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score1" id="score1"  onchange="genarateGrades(this.value)"/> <input id="required1" name="grade1" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course2" id="course2" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score2" id="score2" onchange="genarateGrades1(this.value)" /><input id="required2" name="grade2" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course3" id="course3" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score3" id="score3" onchange="genarateGrades2(this.value)" /><input id="required3" name="grade3" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course4" id="course4" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score4" id="score4" onchange="genarateGrades3(this.value)" /><input id="required4" name="grade4" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course5" id="course5" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score5" id="score5" onchange="genarateGrades4(this.value)" /><input id="required5" name="grade5" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course6" id="course6" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score6" id="score6" onchange="genarateGrades5(this.value)" /><input id="required6" name="grade6" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course7" id="course7" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score7" id="score7" onchange="genarateGrades6(this.value)" /><input id="required7" name="grade7" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" name="course8" id="course8" />
                    </div>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Score:</label>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="text" onblur="aNumber(this.value)" name="score8" id="score8" onchange="genarateGrades7(this.value)" /><input id="required8" name="grade8" class="col-xs-3" readonly />
                    </div>
                </div>
                <div class="btn-group pull-right">
                    <div class="col-md-12 col-sm-6">
                        <input type="submit" value="Save & Generate Report" class="btn btn-block btn-success">
                        <input type="reset" value="Reset" class="btn btn-block btn-default">
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#matric').focus()
    })
</script>
</body>
</html>
