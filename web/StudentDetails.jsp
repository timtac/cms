<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/18/2015
  Time: 10:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%
    if(session != null){
        String status = (String) session.getAttribute("");
        if(status == null){
            response.sendRedirect("");
        }
    }
    else{
        response.sendRedirect("");
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div id="register-content">
            <h3>STUDENT DETAILS</h3>
            <%
                String firstname = (String) session.getAttribute("firstname");
                String lastname = (String) session.getAttribute("lastname");
                String course = (String) session.getAttribute("course");
                String department = (String) session.getAttribute("department");
                String matricNo = (String) session.getAttribute("matricNo");
                String dateOfBirth = (String) session.getAttribute("DateOfBirth");
                String sex = (String) session.getAttribute("sex");
                String address = (String) session.getAttribute("address");

            %>

            <table>
                <tr>
                    <th>First Name:</th>
                    <td><%=firstname%></td>
                </tr>
                <tr>
                    <th>Last Name:</th>
                    <td><%=lastname%></td>
                </tr>
                <tr>
                    <th>Faculty:</th>
                    <td><%=course%></td>
                </tr>
                <tr>
                    <th>Department:</th>
                    <td><%=department%></td>
                </tr>
                <tr>
                    <th>Matric. No.:</th>
                    <td><%=matricNo%></td>
                </tr>
                <tr>
                    <th>Date Of Birth:</th>
                    <td><%=dateOfBirth%></td>
                </tr>
                <tr>
                    <th>Sex:</th>
                    <td><%=sex%></td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td><%=address%></td>
                </tr>
            </table>
            <div class="btn-group pull-right">
                <div class="col-md-12 col-sm-6">
                    <input type="submit" value="Print Report" onclick="printDiv('register-content')" id="prtbtn" class="btn btn-block btn-success">

                </div>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#firstname').focus()
    })
</script>
<style>
    table{
        border: 0px solid #000000;
        width: 100%;
        margin: 10px;
    }
    th {
        border: 0px;
        width: 50%;
        height: 50px;
    }
    td {
        border: 0px;
        width: 50%;
        height: 50px;
    }
</style>
</body>
</html>
