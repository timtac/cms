<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12/4/2015
  Time: 9:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>

    <%
        String username = (String) session.getAttribute("username");
    %>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CollegeManagement</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="EmployeeHome.jsp">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="enrolStudent.jsp">Enrol Student</a></li>
                            <li><a href="CourseRegistration.jsp">Course Reg</a></li>
                            <li><a href="StudentIdentityForm.jsp">Student ID</a></li>

                        </ul>
                    </li>
                    <li><a href="studentFeeForm.jsp">Fee</a></li>
                    <li><a href="AllStudents.jsp">All Students</a></li>
                    <li><a href="GradeStudent.jsp">Grade Students</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="SearchStudents.jsp"> Student</a></li>
                            <li><a href="Search.jsp">Student Result</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><%=username%>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="main-content">
        <div class="pull-right ">
            <select class="col-lg-12">
                <option value="">Search</option>
                <option value="">Student Details</option>
                <option value="">Student Report</option>
            </select>

        </div>
        <div id="register-content">

            <form action="" onsubmit="return studentID()" method="post" class="form-horizontal">
                <h3>Student ID CARD</h3>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Full Name</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="name" id="name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Department</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="department" id="department" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Level:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="level" id="level" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Matric No.:</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="matricNo" id="matric" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-3 col-xs-12">Next Of Kin</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  type="text" name="nxtOfKins" id="kin" />
                    </div>
                </div>

                <div class="btn-group pull-right">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <input type="submit" value="Submit & Generate" id="submit" class="btn btn-success ">
                        <input type="reset" value="Reset" class="btn btn-default ">
                    </div>
                </div>
            </form>

        </div>
    </div>
    <div class="row">


        <div class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>



        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>


        <div  class="col-sm-4">
            <h6>Social Platforms</h6>
            <p><img src="custom/img/social-share/google-plus.png"></p>
            <p><img src="custom/img/social-share/my-space.png"></p>
            <p><img src="custom/img/social-share/picasa.png"></p>
            <p><img src="custom/img/social-share/pinterest.png"></p>
            <p><img src="custom/img/social-share/rss.png"></p>
            <p><img src="custom/img/social-share/linkedin.png"></p>
        </div>

    </div>
    <footer>
        <center>Copyright 2015&copy</center>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#name').focus()
    })
</script>
</body>
</html>

