/**
 * Created by user on 11/19/2015.
 */
    //validate OnSubmit enrolStudent
    function submitStudent(){
    var fname = document.getElementById("firstname").value
    var lname = document.getElementById("lastname").value
    var cour = document.getElementById("course").value
    var dept = document.getElementById("department").value
    var mat = document.getElementById("matricNo").value
    var age = document.getElementById("age").value
    var addr = document.getElementById("address").value
    var img = document.getElementById("file").value

    if(fname == ''){

        alert("Please Fill in Firstname")
        return false;
    }
    else if(lname == ''){
        alert("Please Fill in Lastname")
        return false;
    }
     else if(cour == ""){
        alert("Please Fill in Course")
     return false;
     }
     else if(dept == ""){
        alert("Please Fill in Department")
     return false;
     }
     else if(mat == ""){
        alert("Please Fill in Matric No.")
     return false;
     }
     else if(age == ""){
        alert("Please Fill in Age")
     return false;
     }
     else if(addr == ''){
        alert("Please Fill in Address")
     return false;
     }
     else if(img == ''){
        alert("Please Choose an Image")
     return false;
     }
    return true;
}

//validate OnSubmit registerEmployee
function registerEmployee(){
    var fyst = document.getElementById('firstname').value
    var lyst = document.getElementById('lastname').value
    var pst = document.getElementById('post').value
    var hiDat = document.getElementById('hireDate').value
    var usn = document.getElementById('username').value
    var pasd = document.getElementById('psd').value
    var ag = document.getElementById('age').value
    var add = document.getElementById('address').value
    var img = document.getElementById('file').value

    if(fyst == ''){
        alert('Please Fill Employees\'s First Name')
        return false;
    }
    else if(lyst == ''){
        alert('Please Fill Employees\'s Last Name')
        return false;
    }
    else if(pst == ''){
        alert('Please FIll Employee\'s post')
        return false;
    }
    else if(hiDat == ''){
        alert('Please Fill Hire Date')
        return false;
    }
    else if(usn == ''){
        alert('Please Fill Username')
        return false;
    }
    else if(pasd == ''){
        alert('Please Enter Password')
        return false;
    }
    else if(ag == ''){
        alert('Please Enter Age')
        return false;
    }
    else if(add == ''){
        alert('Please Enter Home Address')
        return false;
    }
    else if(img == ''){
        alert('Choose an Image')
        return false;
    }

    return true;
}
//fields cannot be empty
    function cantBeEmpty(someword) {

        if (someword < 1) {
            alert("This field cannor be empty")
        }

    }

//check if its NaN
    function aNumber(basic){
        if( isNaN(basic)){
        alert("Please enter figures where necessary ")
        }
    }
// validate registerAdmin
    function validateAdmin(){
        var fst = document.getElementById("firstname").value
        var lst = document.getElementById("lastname").value
        var usr = document.getElementById("username").value
        var post = document.getElementById("post").value
        var spsd = document.getElementById("psd").value
        var sage = document.getElementById("age").value
        var sadd = document.getElementById("address").value
        var img = document.getElementById("file").value

        if(fst == ''){
            alert('Enter First-name');
            return false;
        }
        else if(lst == ''){
            alert('Enter Last-name')
            return false;
        }
        else if(usr == ''){
            alert('Enter Username')
            return false;
        }
        else if(post == ''){
            alert('Enter Post')
            return false;
        }
        else if(spsd == ''){
            alert('Enter Password')
            return false;
        }
        else if( sage == ''){
            alert('Enter Age')
            return false;
        }
        else if(sadd == ''){
            alert('Enter Home Address')
            return false;
        }
        else if(img == ''){
            alert('Choose Image')
            return false;
        }
        return true;
    }

//validate OnSubmit CourseRegistration
    function submitCourse(){
        var matricNo = document.getElementById("matric").value
        var lev = document.getElementById("level").value
        var dep = document.getElementById("department").value
        var semes = document.getElementById("semester").value
        var sess = document.getElementById("session").value
        var cour1 = document.getElementById("course1").value
        var unt1 = document.getElementById("unit1").value
        var cour2 = document.getElementById("course2").value
        var unt2 = document.getElementById("unit2").value
        var cour3 = document.getElementById("course3").value
        var unt3 = document.getElementById("unit3").value
        var cour4 = document.getElementById("course4").value
        var unt4 = document.getElementById("unit4").value
        var cour5 = document.getElementById("course5").value
        var unt5 = document.getElementById("unit5").value
        var cour6 = document.getElementById("course6").value
        var unt6 = document.getElementById("unit6").value
        var cour7 = document.getElementById("course7").value
        var unt7 = document.getElementById("unit7").value
        var cour8 = document.getElementById("course8").value
        var unt8 = document.getElementById("unit8").value


        if(matricNo ==''){
            alert('Please Fill in Matric No.')
            return false;
        }
        else if(lev == ''){
            alert('Please Fill in Level')
            return false;
        }
        else if(dep == ''){
            alert('Please Fill in Department')
            return false;
        }
        else if(semes == ''){
            alert('Please Fill in Semester')
            return false;
        }
        else if(sess == ''){
            alert('Please Fill in Session')
            return false;
        }
        else if(cour1 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour2 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour3 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour4 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour5 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour6 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour7 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(cour8 == ''){
            alert('Please Fill in course(s)')
            return false;
        }
        else if(unt1 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt2 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt3 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt4 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt5 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt6 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt7 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }
        else if(unt8 == ''){
            alert('Please Fill in Unit(s)')
            return false;
        }

        return true;
    }



//validate OnSubmit GradeStudent
    function gradeStudent(){
        var matri = document.getElementById('matric').value
        var lev = document.getElementById('level').value
        var depart = document.getElementById('department').value
        var semes = document.getElementById('semester').value
        var ses = document.getElementById('session').value
        var cour1 = document.getElementById('course1').value
        var scor1 = document.getElementById('score1').value
        var cour2 = document.getElementById('course2').value
        var scor2 = document.getElementById('score2').value
        var cour3 = document.getElementById('course3').value
        var scor3 = document.getElementById('score3').value
        var cour4 = document.getElementById('course4').value
        var scor4 = document.getElementById('score4').value
        var cour5 = document.getElementById('course5').value
        var scor5 = document.getElementById('score5').value
        var cour6 = document.getElementById('course6').value
        var scor6 = document.getElementById('score6').value
        var cour7 = document.getElementById('course7').value
        var scor7 = document.getElementById('score7').value
        var cour8 = document.getElementById('course8').value
        var scor8 = document.getElementById('score8').value

        if(matri == ''){
            alert('Enter Matric No.')
            return false
        }
        else if(lev == ''){
            alert('Enter Level')
            return false;
        }
        else if(depart == ''){
            alert('Enter Department')
            return false;
        }
        else if(semes == ''){
            alert('Enter Semester')
            return false;
        }
        else if(ses == ''){
            alert('Enter Session')
            return false;
        }
        else if(cour1 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour2 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour3 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour4 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour5 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour6 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour7 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(cour8 == ''){
            alert('Enter Course(s)')
            return false;
        }
        else if(scor1 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor2 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor3 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor4 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor5 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor6 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor7 == ''){
            alert('Enter score(s)')
            return false;
        }
        else if(scor8 == ''){
            alert('Enter score(s)')
            return false;
        }

        return true;
    }

//validate OnSubmit EmployeeIDForm
    function validateEmpID(){
        var nam = document.getElementById('name').value
        var pos = document.getElementById('post').value
        var empID = document.getElementById('empId').value
        var kin = document.getElementById('kins').value
        var img = document.getElementById('file').value

        if(nam == ''){
            alert('Enter Full Name')
            return false;
        }
        else if(pos == ''){
            alert('Enter Post')
            return false;
        }
        else if(empID == ''){
            alert('Enter Employee\'s ID')
            return false;
        }
        else if(kin == ''){
            alert('Enter Next of Kins')
            return false;
        }
        else if(img == ''){
            alert('Choose Image')
            return false;
        }

        return true;
    }

//validate onSubmit EmployeeSalary
    function employeeSalary(){
        var names = document.getElementById('name').value
        var pst = document.getElementById('post').value
        var pae = document.getElementById('pay').value
        var day = document.getElementById('days').value
        var total = document.getElementById('total').value

        if(names == ''){
            alert('Enter Name')
            return false;
        }
        else if(pst == ''){
            alert('Enter Post')
            return false;
        }
        else if(pae == ''){
            alert('Enter Pay/Day')
            return false;
        }
        else if(day == ''){
            alert('Enter No of Days')
            return false;
        }
        else if(total == ''){
            alert('Calculate Salary!')
            return false;
        }

        return true;
    }

//validate onSubmit Login
    function loginValidate(){
        var usm = document.getElementById('username').value
        var pasd = document.getElementById('psd').value
        var rol = document.getElementById('role').value

        if(usm == ''){
            alert('Enter Username')
            return false;
        }
        else if(pasd == ''){
            alert('Enter Password')
            return false;
        }
        else if(rol ==''){
            alert('Choose Role')
            return false;
        }

        return true
    }

//validate studentFee
    function studentFee(){
        var naem = document.getElementById('name').value
        var depart = document.getElementById('department').value
        var lev = document.getElementById('level').value
        var matri = document.getElementById('matric').value
        var ses = document.getElementById('session').value
        var amtword = document.getElementById('amountWords').value
        var amtFig = document.getElementById('amountFigure').value
        var pay4 = document.getElementById('paymentFor').value
        var reci = document.getElementById('recieved').value

        if(naem == ''){
            alert('Enter Full Name')
            return false;
        }
        else if(depart == ''){
            alert('Enter Department')
            return false;
        }
        else if(lev == ''){
            alert('Enter Level')
            return false;
        }
        else if(matri == ''){
            alert('Enter Matric. No.')
            return false;
        }
        else if(ses == ''){
            alert('Enter Session')
            return false;
        }
        else if(amtword == ''){
            alert('Enter Amount In Words')
            return false;
        }
        else if(amtFig == ''){
            alert('Enter Amount In Figure')
            return false;
        }
        else if(pay4 == ''){
            alert('Enter the payment')
            return false;
        }
        else if(reci == ''){
            alert('Enter Received By.')
            return false
        }

        return true
    }

//validate studentID
    function studentID(){
        var namey = document.getElementById('name').value
        var dep = document.getElementById('department').value
        var lev = document.getElementById('level').value
        var mat = document.getElementById('matric').value
        var kin = document.getElementById('kin').value
        var fil = document.getElementById('file').value

        if(namey == ''){
            alert('Enter Full Name')
            return false;
        }
        else if(dep == ''){
            alert('Enter Department')
            return false;
        }
        else if(lev == ''){
            alert('Enter Level')
            return false;
        }
        else if(mat == ''){
            alert('Enter Matric. No.')
            return false;
        }
        else if(kin == ''){
            alert('Enter Session')
            return false;
        }
        else if(fil == 1){
            alert('Choose Image')
            return false;
        }

        return true;
    }
//Generate grades from A-D
    function genarateGrades(score){
        var field = document.getElementById("required1")
        field.value = getScore(score)
    }
    function genarateGrades1(score){
        var field = document.getElementById("required2")
        field.value = getScore(score)
    }
    function genarateGrades2(score){
        var field = document.getElementById("required3")
        field.value = getScore(score)
    }
    function genarateGrades3(score){
        var field = document.getElementById("required4")
        field.value = getScore(score)
    }
    function genarateGrades4(score){
        var field = document.getElementById("required5")
        field.value = getScore(score)
    }
    function genarateGrades5(score){
        var field = document.getElementById("required6")
        field.value = getScore(score)
    }
    function genarateGrades6(score){
        var field = document.getElementById("required7")
        field.value = getScore(score)
    }
    function genarateGrades7(score){
        var field = document.getElementById("required8")
        field.value = getScore(score)
    }

    function getScore(mark){
        var grade;
        if(mark >= 70 && mark <= 100){
            grade = 'A';
        }

        if( mark>= 60 && mark <= 69){
            grade = 'B';
        }

        if(mark >= 50 && mark <= 59){
            grade = 'C';
        }
        if(mark >= 40 && mark <= 49){
            grade = 'D';
        }

        if(mark >= 35 && mark <= 39){
            grade = 'E';
        }
        else if(mark >=0 && mark <=34){
            grade = 'F';
        }
        else if(mark >100){
          grade =  alert('Enter a valid score within 0-100')
        }
        else if(mark <0){
          grade =  alert('Enter a valid score within 0-100')
        }

        return grade;
    }

//course names auto-generate
    /*function courseNames(course) {
        var course1 = document.getElementById("")
        var course2 = document.getElementById("")
        var course3 = document.getElementById("")
        var course4 = document.getElementById("")
        var course5 = document.getElementById("")
        var course6 = document.getElementById("")
        var course7 = document.getElementById("")
        var course8 = document.getElementById("")

        if (course == "computerScience") {
            course1.value = ""
            course2.value = ""
            course3.value = ""
            course4.value = ""
            course5.value = ""
            course6.value = ""
            course7.value = ""
            course8.value = ""
        }

        if (course == "accounting") {
            course1.value = ""
            course2.value = ""
            course3.value = ""
            course4.value = ""
            course5.value = ""
            course6.value = ""
            course7.value = ""
            course8.value = ""
        }

        //get Role for action attribute for login
    }*/
// get Employee role
    function getRole(){
        var role = document.getElementById('role').value

        //var url;

        if(role == 1){
            document.getElementById('loginForm').action = "/admin/login"
        }
        else if(role == 2){
            document.getElementById('loginForm').action = "/employee"
        }

    }

    //calculate salary
    function calculatePayment(){
        var pay = document.getElementById('pay').value;
        var days = document.getElementById('days').value;
        var bonus = document.getElementById('bonus').value;
        var total = document.getElementById('total');

        if(pay != '' && days != ''){
            total.value = (pay * days) + +bonus;
        }
        return total.value;
    }

//calculate unit course
    function calculateUnit(){
        var uni1 = document.getElementById('unit1').value
        var uni2 = document.getElementById('unit2').value
        var uni3 = document.getElementById('unit3').value
        var uni4 = document.getElementById('unit4').value
        var uni5 = document.getElementById('unit5').value
        var uni6 = document.getElementById('unit6').value
        var uni7 = document.getElementById('unit7').value
        var uni8 = document.getElementById('unit8').value
        var tot = document.getElementById('total')

        if(uni1 != '' && uni2 != ''){
        tot.value = parseInt(uni1) + parseInt(uni2) + parseInt(uni3) + parseInt(uni4) + parseInt(uni5) + parseInt(uni6) + parseInt(uni7) + parseInt(uni8)
        }
        else{
            alert('Enter unit values')
        }
        return tot.value
    }

// Print page
    function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML =
        "<html><head><title></title><style type='text/css'>#prtbtn{display: none;} #rstbtn{display: none;}</style></head><body>" +
        divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;


    }



