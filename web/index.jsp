<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11/19/2015
  Time: 10:22 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head><title>College Management System</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="custom/custom.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="custom/custom.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
<div>

    <header><img src="custom/img/schoolsys-Banner.png"></header>




    <div id="main-content">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#"><i class="fa fa-home">CollegeManagementSystem</i></a></li>
                        <li><a href="registerAdmin.jsp"><i class="fa fa-user">Admin Registration</i></a></li>

                        <li><a href="Login.jsp"><i class="fa fa-user"> Login</i></a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </div>


        <div class="row">


            <div class="col-sm-4">
                <h6>Social Platforms</h6>
                <p><img src="custom/img/social-share/google-plus.png"></p>
                <p><img src="custom/img/social-share/my-space.png"></p>
                <p><img src="custom/img/social-share/picasa.png"></p>
                <p><img src="custom/img/social-share/pinterest.png"></p>
                <p><img src="custom/img/social-share/rss.png"></p>
                <p><img src="custom/img/social-share/linkedin.png"></p>
            </div>



            <div  class="col-sm-4">
                <h6>Social Platforms</h6>
                <p><img src="custom/img/social-share/google-plus.png"></p>
                <p><img src="custom/img/social-share/my-space.png"></p>
                <p><img src="custom/img/social-share/picasa.png"></p>
                <p><img src="custom/img/social-share/pinterest.png"></p>
                <p><img src="custom/img/social-share/rss.png"></p>
                <p><img src="custom/img/social-share/linkedin.png"></p>
            </div>


            <div  class="col-sm-4">
                <h6>Social Platforms</h6>
                <p><img src="custom/img/social-share/google-plus.png"></p>
                <p><img src="custom/img/social-share/my-space.png"></p>
                <p><img src="custom/img/social-share/picasa.png"></p>
                <p><img src="custom/img/social-share/pinterest.png"></p>
                <p><img src="custom/img/social-share/rss.png"></p>
                <p><img src="custom/img/social-share/linkedin.png"></p>
            </div>

        </div>
    <footer >
        <div class="footee">
            <center><span class="copy">Copyright &copy 2015</span></center>
        </div>
    </footer>

</div>
<script>
    $(document).ready(function(){
        $('#username').focus()
    })

</script>
</body>
</html>
