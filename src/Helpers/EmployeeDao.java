package Helpers;

import Models.Employee;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by user on 11/20/2015.
 */
public class EmployeeDao {
    JdbcPooledConnectionSource con;
    Models.Employee employee;

    public EmployeeDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public EmployeeDao(Models.Employee employee) throws Exception{
        this.employee = employee;

        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void createEmployee() throws Exception{
        Dao<Models.Employee, String> empDao = DaoManager.createDao(con,Models.Employee.class);
        empDao.createIfNotExists(employee);
    }

    public boolean authenticateEmployee(String username, String password) throws Exception {
        Dao<Employee,String> empDao = DaoManager.createDao(con,Employee.class);
        List<Employee> list = empDao.queryForAll();
        for(int i = 0; i < list.size(); i++){
            employee = list.get(i);
            if((employee.getUserId().equals(username)) && (employee.getPassword().equals(password))){
                return true;
            }
        }
        return false;
    }

    public List<Employee> getEmployeeDetails(String username) throws Exception {
        Dao<Employee,String> empDao = DaoManager.createDao(con,Employee.class);
        return empDao.queryForEq(employee.getUserId(),username);
    }

    public List<Employee> allEmployees() throws Exception{
        Dao<Employee,String> empDao = DaoManager.createDao(con,Employee.class);
        return empDao.queryForAll();
    }

}
