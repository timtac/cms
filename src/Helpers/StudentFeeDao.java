package Helpers;

import Models.StudentFeeForm;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

/**
 * Created by user on 12/4/2015.
 */
public class StudentFeeDao {
    JdbcPooledConnectionSource con;
    StudentFeeForm feeForm;

    public StudentFeeDao(StudentFeeForm feeForm) throws Exception{
        this.feeForm = feeForm;

        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public StudentFeeDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void createFee() throws Exception{
        Dao<StudentFeeForm,String> feeDao = DaoManager.createDao(con,StudentFeeForm.class);
        feeDao.create(feeForm);
    }
}
