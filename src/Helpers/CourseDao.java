package Helpers;

import Models.CourseRegistration;
import Models.Student;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by user on 11/23/2015.
 */
public class CourseDao {
    JdbcPooledConnectionSource con;
    CourseRegistration course;

    public CourseDao() throws Exception{
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public CourseDao(CourseRegistration course) throws Exception{
        this.course = course;
        con = new JdbcPooledConnectionSource(DbConstant.DB_URL);
        con.setUsername(DbConstant.DB_USER);
        con.setPassword(DbConstant.DB_PWD);
    }

    public void registerCourse() throws Exception {
        Dao<CourseRegistration,String > courseDao = DaoManager.createDao(con,CourseRegistration.class);
        courseDao.create(course);
    }

    public List<CourseRegistration> queryRegisteredCourse(String matricNo, String level) throws Exception {
        Dao<CourseRegistration,String> courseDao = DaoManager.createDao(con,CourseRegistration.class);
        QueryBuilder<CourseRegistration,String> queryBuilder = courseDao.queryBuilder();
        Where<CourseRegistration,String> where = queryBuilder.where();
        where.eq(matricNo,course.getStudent_matric_no());
        where.and();
        where.eq(course.getLevel(),level);
        PreparedQuery<CourseRegistration> prepare = queryBuilder.prepare();

        return courseDao.query(prepare);
    }
}
