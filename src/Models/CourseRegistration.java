package Models;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
/**
 * Created by user on 11/21/2015.
 */
@DatabaseTable(tableName = "course_registration")
public class CourseRegistration {
    @DatabaseField(foreign = true,columnName = "student_matric_no",canBeNull = false)
    private Student student_matric_no;
    @DatabaseField
    private String level;
    @DatabaseField
    private String semester;
    @DatabaseField
    private String contactSession;
    @DatabaseField
    private String department;
    @DatabaseField
    private String course1;
    @DatabaseField
    private String unit1;
    @DatabaseField
    private String course2;
    @DatabaseField
    private String unit2;
    @DatabaseField
    private String course3;
    @DatabaseField
    private String unit3;
    @DatabaseField
    private String course4;
    @DatabaseField
    private String unit4;
    @DatabaseField
    private String course5;
    @DatabaseField
    private String unit5;
    @DatabaseField
    private String course6;
    @DatabaseField
    private String unit6;
    @DatabaseField
    private String course7;
    @DatabaseField
    private String unit7;
    @DatabaseField
    private String course8;
    @DatabaseField
    private String unit8;
    @DatabaseField
    private String totalUnit;

    public CourseRegistration(){

    }

    public Student getStudent_matric_no() {
        return student_matric_no;
    }

    public void setStudent_matric_no(Student student_matric_no) {
        this.student_matric_no = student_matric_no;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getContactSession() {
        return contactSession;
    }

    public void setContactSession(String contactSession) {
        this.contactSession = contactSession;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCourse1() {
        return course1;
    }

    public void setCourse1(String course1) {
        this.course1 = course1;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public String getCourse2() {
        return course2;
    }

    public void setCourse2(String course2) {
        this.course2 = course2;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public String getCourse3() {
        return course3;
    }

    public void setCourse3(String course3) {
        this.course3 = course3;
    }

    public String getUnit3() {
        return unit3;
    }

    public void setUnit3(String unit3) {
        this.unit3 = unit3;
    }

    public String getCourse4() {
        return course4;
    }

    public void setCourse4(String course4) {
        this.course4 = course4;
    }

    public String getUnit4() {
        return unit4;
    }

    public void setUnit4(String unit4) {
        this.unit4 = unit4;
    }

    public String getCourse5() {
        return course5;
    }

    public void setCourse5(String course5) {
        this.course5 = course5;
    }

    public String getUnit5() {
        return unit5;
    }

    public void setUnit5(String unit5) {
        this.unit5 = unit5;
    }

    public String getCourse6() {
        return course6;
    }

    public void setCourse6(String course6) {
        this.course6 = course6;
    }

    public String getUnit6() {
        return unit6;
    }

    public void setUnit6(String unit6) {
        this.unit6 = unit6;
    }

    public String getCourse7() {
        return course7;
    }

    public void setCourse7(String course7) {
        this.course7 = course7;
    }

    public String getUnit7() {
        return unit7;
    }

    public void setUnit7(String unit7) {
        this.unit7 = unit7;
    }

    public String getCourse8() {
        return course8;
    }

    public void setCourse8(String course8) {
        this.course8 = course8;
    }

    public String getUnit8() {
        return unit8;
    }

    public void setUnit8(String unit8) {
        this.unit8 = unit8;
    }

    public String getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(String totalUnit) {
        this.totalUnit = totalUnit;
    }
}
