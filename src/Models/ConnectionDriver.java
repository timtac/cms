package Models;

import java.sql.*;
import java.util.Properties;

/**
 * Created by user on 1/6/2016.
 */
public class ConnectionDriver {
    private ConnectionDriver(){

    }

    public static Connection getConnection(String url) throws SQLException{
        DriverManager.setLoginTimeout(10);
        return DriverManager.getConnection(url);
    }

    public static Connection getConnection(String url,String username,String password) throws SQLException {

        return DriverManager.getConnection(url,username,password);
    }
    public static Connection getConnection(String url,Properties prop) throws SQLException{
        return DriverManager.getConnection(url,prop);
    }

    public static void close(Connection connection){
        if(connection != null) {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void close(Statement statement){
        if(statement != null){
            try{
                statement.close();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public static void close(ResultSet resultSet){
        if (resultSet != null){
            try{
                resultSet.close();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
