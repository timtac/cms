package Models;

/**
 * Created by user on 12/8/2015.
 */
public class EmployeeId {

    private String fullname;
    private String postHeld;
    private String empIdNo;
    private String nextOfKin;
    private String image;

    public EmployeeId(){

    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPostHeld() {
        return postHeld;
    }

    public void setPostHeld(String postHeld) {
        this.postHeld = postHeld;
    }

    public String getEmpIdNo() {
        return empIdNo;
    }

    public void setEmpIdNo(String empIdNo) {
        this.empIdNo = empIdNo;
    }

    public String getNextOfKin() {
        return nextOfKin;
    }

    public void setNextOfKin(String nextOfKin) {
        this.nextOfKin = nextOfKin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
