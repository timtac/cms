package Models;

/**
 * Created by user on 12/12/2015.
 */
public class StudentId {
    private String name;
    private String department;
    private String level;
    private String matricNo;
    private String nxtOfKins;
    private String image;

    public StudentId(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMatricNo() {
        return matricNo;
    }

    public void setMatricNo(String matricNo) {
        this.matricNo = matricNo;
    }

    public String getNxtOfKins() {
        return nxtOfKins;
    }

    public void setNxtOfKins(String nxtOfKins) {
        this.nxtOfKins = nxtOfKins;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
