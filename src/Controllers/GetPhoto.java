package Controllers;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ResourceBundle;

/**
 * Created by user on 12/28/2015.
 */
@WebServlet("/getImage")
public class GetPhoto extends HttpServlet {
    static final long serialVersionUID = 1L;
    ResourceBundle props= null;
    private static final int BUFSIZE = 100;
    private ServletContext servletContext;

    public GetPhoto(){
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        sendImage(getServletContext(),request,response);
    }

    public void sendImage(ServletContext servletContext,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        this.servletContext = servletContext;
        String fullfilePath = "C:/Users/user/IdeaProjects/CollegeManagementSystem/out/artifacts/CollegeManagementSystem_war_exploded/images/IMG_0044.JPG";// path from database here
        doShowImageOnPage(fullfilePath,request,response);
        doDownload(fullfilePath,request,response);
    }

    private void doShowImageOnPage(String fullfilePath,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        response.reset();
        response.setHeader("Content-Disposition","inline");
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Expires","0");
        response.setContentType("image/tiff");
        byte[] image = getImage(fullfilePath);
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(image);
        outputStream.close();
    }

    public void doDownload(String fullfilePath, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        File filename = new File(fullfilePath);
        int length = 0;
        ServletOutputStream servletOutputStream = response.getOutputStream();
        //ServletContext servletContext1 = getServletConfig().getServletContext();
        ServletContext context = servletContext;
        String mimetype = context.getMimeType(fullfilePath);
        response.setContentType((mimetype != null) ? mimetype:"application/octet-stream");
        response.setContentLength((int)filename.length());
        byte[] bbuf = new byte[BUFSIZE];
        DataInputStream in = new DataInputStream(new FileInputStream(filename));

        while((in != null) && ((length = in.read(bbuf)) != -1)){
            servletOutputStream.write(bbuf,0,length);
        }
        in.close();
        servletOutputStream.flush();
        servletOutputStream.close();
    }

    private byte[] getImage(String filename){
        byte[] result = null;
        String fileLocation = filename;
        File f = new File(fileLocation);
        result = new byte[(int)f.length()];

        try {
            FileInputStream fileInputStream = new FileInputStream(fileLocation);
            fileInputStream.read(result);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }
}
