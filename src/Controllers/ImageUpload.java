package Controllers;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


/**
 * Created by user on 1/2/2016.
 */
@WebServlet("/UploadDownloadImage")
@MultipartConfig
public class ImageUpload extends HttpServlet {
    private static final long serialVersionUID = 1L;



    public void doGet(HttpServletRequest request,HttpServletResponse response ) throws ServletException, IOException {
                String fileName = request.getParameter("fileName");
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                throw new ServletException("File doesn't exists on server.");
            }
            //System.out.println("File location on server::" + file.getAbsolutePath());
            ServletContext ctx = getServletContext();
            InputStream fis = new FileInputStream(file);
            String mimeType = ctx.getMimeType(file.getAbsolutePath());
            response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            ServletOutputStream os = response.getOutputStream();
            byte[] bufferData = new byte[1024];
            int read = 0;
            while ((read = fis.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            }
            os.flush();
            os.close();
            fis.close();
            // System.out.println("File downloaded at client successfully");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
