package Controllers;

import Helpers.EmployeeDao;
import Models.Employee;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 12/25/2015.
 */
@WebServlet("/allEmployee")
public class AllEmpServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try{
            EmployeeDao employeeDao = new EmployeeDao();
            List<Employee> list = employeeDao.allEmployees();

            JsonArray jsonArray = new JsonArray();

            for(int count=0;count < list.size(); count++){

                Employee employee = list.get(count);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("sn",employee.getEmp_Id());
                jsonObject.addProperty("firstname", employee.getFirstName());
                jsonObject.addProperty("lastname",employee.getLastName());
                jsonObject.addProperty("post",employee.getPost());
                jsonObject.addProperty("hire",employee.getHireDate());
                jsonObject.addProperty("userId", employee.getUserId());
                jsonObject.addProperty("age",employee.getAge());
                jsonObject.addProperty("sex",employee.getSex());
                jsonObject.addProperty("address",employee.getAddress());

                jsonArray.add(jsonObject);
            }
            out.println(jsonArray.toString());
        }
        catch(Exception ex){

        }
    }
}
