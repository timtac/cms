package Controllers;

import Helpers.AdminDao;
import Models.Admin;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 12/8/2015.
 */
@WebServlet("/allAdmin")
public class GetAdminServlet extends HttpServlet{
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try{
            AdminDao adminDao = new AdminDao();
            List<Admin> list = adminDao.allAdmin();

            JsonArray jsonArray = new JsonArray();
            for(int count=0; count < list.size();count++){
                Admin admin = list.get(count);

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("no",admin.getAdminId());
                jsonObject.addProperty("firstname",admin.getFirstName());
                jsonObject.addProperty("lastname",admin.getLastName());
                jsonObject.addProperty("username",admin.getUsername());
                jsonObject.addProperty("sex",admin.getSex());
                jsonObject.addProperty("post",admin.getPost());
                jsonObject.addProperty("age",admin.getAge());
                jsonObject.addProperty("Address",admin.getAddress());

                jsonArray.add(jsonObject);
            }
                out.println(jsonArray.toString());
        }
        catch(Exception ex){

        }
    }
}
