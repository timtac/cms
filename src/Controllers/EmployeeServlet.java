package Controllers;

import Helpers.EmployeeDao;
import Models.Employee;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.sql.*;

/**
 * Created by user on 11/21/2015.
 */
@WebServlet("/employee")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class EmployeeServlet extends HttpServlet {
    Connection connection = null;
    Statement statement = null;

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String post = request.getParameter("post");
        String hireDate = request.getParameter("hireDate");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);
        HttpSession session = request.getSession();

        try{
            EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
            String nwpsd = crypto.encrypt(username,password);

            Models.Employee employee = new Models.Employee();
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setPost(post);
            employee.setHireDate(hireDate);
            employee.setUserId(username);
            employee.setPassword(nwpsd);
            employee.setAddress(address);
            employee.setAge(age);
            employee.setSex(sex);
            String filePath = savePath + File.separator + fileName;
            employee.setPhoto(filePath);

            Helpers.EmployeeDao employeeDao = new EmployeeDao(employee);
            employeeDao.createEmployee();

            session.setAttribute("employee","true");
            session.setAttribute("firstname",firstName);
            session.setAttribute("last",lastName);
            session.setAttribute("post",post);
            session.setAttribute("username",username);
            session.setAttribute("password",password);
            session.setAttribute("address",address);
            session.setAttribute("age",age);
            session.setAttribute("sex",sex);
            session.setAttribute("image",filePath);
            response.sendRedirect("/EmployeeReport.jsp");
        }
        catch (Exception ex){
            response.sendRedirect("/registerEmployees.jsp");
        }
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

            try{
                EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
                String psd = crypto.encrypt(username,password);

                Helpers.EmployeeDao employeeDao = new EmployeeDao();

                boolean result = employeeDao.authenticateEmployee(username,psd);

                if(result == true){
                    Class.forName("com.mysql.jdbc.Driver");
                    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/college_management_system","root","iamagod$#12");
                    statement = connection.createStatement();

                    ResultSet resultSet = statement.executeQuery("select * from employee where userId = '"+username+"'");
                    JsonArray jsonArray = new JsonArray();
                    while(resultSet.next()){
                        JsonObject jsonObject = new JsonObject();
                        session.setAttribute("id",resultSet.getObject(1));
                        session.setAttribute("first-name",resultSet.getObject(2));
                        session.setAttribute("last-name",resultSet.getObject(3));
                        session.setAttribute("post", resultSet.getObject(4));
                        session.setAttribute("hire", resultSet.getObject(5));
                        session.setAttribute("username", resultSet.getObject(6));
                        session.setAttribute("sex", resultSet.getObject(8));
                        session.setAttribute("age", resultSet.getObject(9));
                        session.setAttribute("address", resultSet.getObject(10));
                        session.setAttribute("photo",resultSet.getObject(11));

                        jsonArray.add(jsonObject);
                    }
                    session.setAttribute("employeelogin","true");
                    response.sendRedirect("/EmployeeHome.jsp");
                }
                else{

                }

            }
            catch(Exception ex){
                ex.printStackTrace();
                response.sendRedirect("Login.jsp");
            }

    }

}
