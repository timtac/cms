package Controllers;

import Helpers.AdminDao;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;



/**
 * Created by user on 11/20/2015.
 */
@WebServlet("/admin")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class AdminCreate extends HttpServlet{


   // private static final long serialVersionUID = 1L;
    private static final String SAVE_DIR = "images";
    private ServletFileUpload uploader =null;

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String post = request.getParameter("post");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

            try{

                HttpSession session = request.getSession();

                EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
                String nwpsd = crypto.encrypt(username, password);

                Models.Admin admin = new Models.Admin();
                admin.setFirstName(firstName);
                admin.setLastName(lastName);
                admin.setUsername(username);
                admin.setPassword(nwpsd);
                admin.setAge(age);
                admin.setSex(sex);
                admin.setPost(post);
                admin.setAddress(address);
                String filePath = savePath + File.separator + fileName;
                admin.setPhoto(filePath);
                AdminDao adminDao = new AdminDao(admin);
                adminDao.createAdmin();

                session.setAttribute("", "");
                session.setAttribute("firstname", firstName);
                session.setAttribute("lastname",lastName);
                session.setAttribute("post",post);
                session.setAttribute("age",age);
                session.setAttribute("sex",sex);
                session.setAttribute("address",address);
                session.setAttribute("image",filePath);
                session.setAttribute("username",username);
                response.setContentType("text/html");
                PrintWriter writer = response.getWriter();
                writer.println("<html><body>");
                writer.println("Done!!!");
                writer.println("<a href='Login.jsp'>Login Here</a></body></html>");
                writer.close();


            } catch (FileUploadException ex) {
                ex.printStackTrace();
            }
            catch (Exception ex){

            }

    }




}
