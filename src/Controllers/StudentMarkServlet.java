package Controllers;

import Helpers.StudentDao;
import Helpers.StudentMarkDao;
import Models.Student;
import Models.StudentMark;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.sql.*;

/**
 * Created by user on 11/26/2015.
 */
@WebServlet("/mark")
public class StudentMarkServlet extends HttpServlet {

    Connection connection = null;
    Statement statement = null;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        String matricNo = request.getParameter("matricNo");
        String level = request.getParameter("level");
        String semester = request.getParameter("semester");
        String department = request.getParameter("department");
        String contactSession = request.getParameter("session");
        String course1 = request.getParameter("course1");
        String score1 = request.getParameter("score1");
        String grade1 = request.getParameter("grade1");
        String course2 = request.getParameter("course2");
        String score2 = request.getParameter("score2");
        String grade2 = request.getParameter("grade2");
        String course3 = request.getParameter("course3");
        String score3 = request.getParameter("score3");
        String grade3 = request.getParameter("grade3");
        String course4 = request.getParameter("course4");
        String score4 = request.getParameter("score4");
        String grade4 = request.getParameter("grade4");
        String course5 = request.getParameter("course5");
        String score5 = request.getParameter("score5");
        String grade5 = request.getParameter("grade5");
        String course6 = request.getParameter("course6");
        String score6 = request.getParameter("score6");
        String grade6 = request.getParameter("grade6");
        String course7 = request.getParameter("course7");
        String score7 = request.getParameter("score7");
        String grade7 = request.getParameter("grade7");
        String course8 = request.getParameter("course8");
        String score8 = request.getParameter("score8");
        String grade8 = request.getParameter("grade8");

        HttpSession session = request.getSession();

        try{
            Student student = new Student();
            student.setMatricNo(matricNo);
            StudentDao studentDao = new StudentDao(student);
            studentDao.refreshStudent();

            StudentMark studentMark = new StudentMark();
            studentMark.setStudent_matric_no(student);
            studentMark.setLevel(level);
            studentMark.setSemester(semester);
            studentMark.setDepartment(department);
            studentMark.setContactSession(contactSession);
            studentMark.setCourse1(course1);
            studentMark.setScore1(score1);
            studentMark.setGrade1(grade1);
            studentMark.setCourse2(course2);
            studentMark.setScore2(score2);
            studentMark.setGrade2(grade2);
            studentMark.setCourse3(course3);
            studentMark.setScore3(score3);
            studentMark.setGrade3(grade3);
            studentMark.setCourse4(course4);
            studentMark.setScore4(score4);
            studentMark.setGrade4(grade4);
            studentMark.setCourse5(course5);
            studentMark.setScore5(score5);
            studentMark.setGrade5(grade5);
            studentMark.setCourse6(course6);
            studentMark.setScore6(score6);
            studentMark.setGrade6(grade6);
            studentMark.setCourse7(course7);
            studentMark.setScore7(score7);
            studentMark.setGrade7(grade7);
            studentMark.setCourse8(course8);
            studentMark.setScore8(score8);
            studentMark.setGrade8(grade8);

            StudentMarkDao studentMarkDao = new StudentMarkDao(studentMark);
            studentMarkDao.setStudentScores();


            session.setAttribute("mark","true");
            session.setAttribute("matricNo",matricNo);
            session.setAttribute("level",level);
            session.setAttribute("semester",semester);
            session.setAttribute("department",department);
            session.setAttribute("session",contactSession);
            session.setAttribute("course1",course1);
            session.setAttribute("grade1",grade1);
            session.setAttribute("course2",course2);
            session.setAttribute("grade2",grade2);
            session.setAttribute("course3",course3);
            session.setAttribute("grade3",grade3);
            session.setAttribute("course4",course4);
            session.setAttribute("grade4",grade4);
            session.setAttribute("course5",course5);
            session.setAttribute("grade5",grade5);
            session.setAttribute("course6",course6);
            session.setAttribute("grade6",grade6);
            session.setAttribute("course7",course7);
            session.setAttribute("grade7",grade7);
            session.setAttribute("course8",course8);
            session.setAttribute("grade8",grade8);

            response.sendRedirect("/StudentReport.jsp");
        }
        catch(Exception ex){
            response.sendRedirect("/GradeStudent.jsp");
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String matricNo = request.getParameter("matricNo");
        String semester = request.getParameter("semester");
            HttpSession session = request.getSession();
        try{

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/college_management_system","root","iamagod$#12");
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from student_mark where student_matric_no = '"+matricNo+"' and semester = '"+semester+"' ");

            JsonArray jsonArray = new JsonArray();
            while(resultSet.next()){

                JsonObject jsonObject = new JsonObject();
                session.setAttribute("matricNo",resultSet.getObject(1));
                session.setAttribute("level",resultSet.getObject(2));
                session.setAttribute("semester",resultSet.getObject(3));
                session.setAttribute("contactSession",resultSet.getObject(5));
                session.setAttribute("department",resultSet.getObject(4));
                session.setAttribute("course1",resultSet.getObject(6));
                session.setAttribute("grade1",resultSet.getObject(8));
                session.setAttribute("course2",resultSet.getObject(9));
                session.setAttribute("grade2",resultSet.getObject(11));
                session.setAttribute("course3",resultSet.getObject(12));
                session.setAttribute("grade3",resultSet.getObject(14));
                session.setAttribute("course4",resultSet.getObject(15));
                session.setAttribute("grade4",resultSet.getObject(17));
                session.setAttribute("course5",resultSet.getObject(18));
                session.setAttribute("grade5",resultSet.getObject(20));
                session.setAttribute("course6",resultSet.getObject(21));
                session.setAttribute("grade6",resultSet.getObject(23));
                session.setAttribute("course7",resultSet.getObject(24));
                session.setAttribute("grade7",resultSet.getObject(26));
                session.setAttribute("course8",resultSet.getObject(27));
                session.setAttribute("grade8",resultSet.getObject(29));

                //jsonArray.add(jsonObject);

            }
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            //out.println(jsonArray.toString());

            response.sendRedirect("/StudentReportBySearch.jsp");

        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
