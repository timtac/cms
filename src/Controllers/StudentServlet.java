package Controllers;

import Helpers.StudentDao;
import Models.Student;
import java.sql.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 11/21/2015.
 */
@WebServlet("/student")
@MultipartConfig(fileSizeThreshold = 1024*1024*2, maxFileSize =1024*1024*10, maxRequestSize = 1024*1024*50)
public class StudentServlet extends HttpServlet {
    Connection connection =null;
    Statement statement = null;

    private static final String SAVE_DIR = "images";

    public String extractFileName(Part part){
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for(String s: items){
            if(s.trim().startsWith("filename")){
                return s.substring(s.indexOf("=") + 2, s.length() - 1 );
            }
        }
        return "";
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        //gets absolute path for the web application
        String appPath = request.getServletContext().getRealPath("");
        // construct path of the directory to save uploaded path
        String savePath = appPath + File.separator + SAVE_DIR ;

        //creates the save directory if it does not exists
        File filedir = new File(savePath);
        if(!filedir.exists()){
            filedir.mkdir();
        }

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String course = request.getParameter("course");
        String department = request.getParameter("department");
        String age = request.getParameter("age");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        String matricNo = request.getParameter("matricNo");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);
        part.write(savePath + File.separator + fileName);

            HttpSession session = request.getSession();
        try{
            Student student = new Student();
            student.setFirstName(firstName);
            student.setLastName(lastName);
            student.setFaculty(course);
            student.setDepartment(department);
            student.setMatricNo(matricNo);
            student.setSex(sex);
            student.setAddress(address);
            student.setAge(age);
            String filePath = savePath + File.separator + fileName;
            student.setPhoto(filePath);

            Helpers.StudentDao  studentDao = new StudentDao(student);
            studentDao.createStudent();

            session.setAttribute("","");
            session.setAttribute("firstname",firstName);
            session.setAttribute("lastname",lastName);
            session.setAttribute("course",course);
            session.setAttribute("department",department);
            session.setAttribute("matricNo",matricNo);
            session.setAttribute("sex",sex);
            session.setAttribute("address",address);
            session.setAttribute("age",age);
            session.setAttribute("image",filePath);

        }catch(Exception ex){

        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        String matricNo = request.getParameter("matricNo");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/college_management_system","root","iamagod$#12");
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from student where matricNo = '"+matricNo+"'");

            JsonArray jsonArray = new JsonArray();
            while(resultSet.next()){
                JsonObject jsonObject = new JsonObject();
                session.setAttribute("firstname",resultSet.getObject(1));
                session.setAttribute("lastname",resultSet.getObject(2));
                session.setAttribute("course", resultSet.getObject(3));
                session.setAttribute("department", resultSet.getObject(4));
                session.setAttribute("matricNo", resultSet.getObject(5));
                session.setAttribute("DateOfBirth", resultSet.getObject(6));
                session.setAttribute("sex", resultSet.getObject(7));
                session.setAttribute("address", resultSet.getObject(8));

                jsonArray.add(jsonObject);
            }

            //out.println(jsonArray.toString());
            response.sendRedirect("/StudentDetails.jsp");
        }
        catch(Exception ex){

            response.sendRedirect("/S");
        }
    }
}
