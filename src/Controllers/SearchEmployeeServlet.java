package Controllers;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
/**
 * Created by user on 12/18/2015.
 */
@WebServlet("/searchEmployee")
public class SearchEmployeeServlet extends HttpServlet {

    Connection connection = null;
    Statement statement = null;

    public void doGet(HttpServletRequest request,HttpServletResponse response){

        String name = request.getParameter("name");

        HttpSession session = request.getSession();

        try{

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/college_management_system","root","iamagod$#12");
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from employee where firstName = '"+name+"'");

            while(resultSet.next()){
                session.setAttribute("firstname", resultSet.getObject(2));
                session.setAttribute("lastname",resultSet.getObject(3));
                session.setAttribute("post",resultSet.getObject(4));
                session.setAttribute("hireDate",resultSet.getObject(5));
                session.setAttribute("userId",resultSet.getObject(6));
                session.setAttribute("sex",resultSet.getObject(8));
                session.setAttribute("age",resultSet.getObject(9));
                session.setAttribute("address",resultSet.getObject(10));
            }
            session.setAttribute("search","true");

            response.sendRedirect("/EmployeeDetails.jsp");
        }
        catch(Exception ex){

        }
    }
}
