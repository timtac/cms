package Controllers;

import Helpers.AdminDao;
import Models.ConnectionDriver;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by user on 1/7/2016.
 */
@WebServlet("/admin/login")
public class AdminLogOn  extends HttpServlet{

    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet;

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
        doGet(request,response);
    }
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        try{
            RequestDispatcher dispatcher;
            ServletContext context = getServletContext();
            EncrytHelper.EncrytoClass crypto = new EncrytHelper.EncrytoClass();
            String psd = crypto.encrypt(username,password);

            AdminDao adminDao = new AdminDao();


            boolean result = adminDao.authenticateAdmin(username,psd);

            if(result == true){

                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/college_management_system", "root", "iamagod$#12");
                statement = connection.createStatement();

                resultSet = statement.executeQuery("select * from admin where username = '"+username+"'");

                while(resultSet.next()) {

                    request.setAttribute("admin", resultSet.getString(1));
                    session.setAttribute("firstname", resultSet.getString(2));
                    session.setAttribute("lastname", resultSet.getString(3));
                    session.setAttribute("post", resultSet.getString(4));
                    session.setAttribute("username", resultSet.getString(5));
                    session.setAttribute("sex", resultSet.getString(8));
                    session.setAttribute("age", resultSet.getString(7));
                    session.setAttribute("address", resultSet.getString(9));
                    session.setAttribute("photo", resultSet.getString(10));

                    dispatcher = context.getRequestDispatcher("/AdminHome.jsp");
                    if(dispatcher == null){
                        dispatcher = context.getRequestDispatcher("/Error404.jsp");
                    }
                    session.setAttribute("adminlogin", "true");
                    dispatcher.forward(request,response);
                    //response.sendRedirect("/AdminHome.jsp");
                }

            } else{
                response.setContentType("text/html");

            }


        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            try{
                ConnectionDriver.close(resultSet);
                ConnectionDriver.close(statement);
                ConnectionDriver.close(connection);
            }
            catch(Exception ex){

            }
        }

    }
}
